import os
import numpy as np
import shutil
import time
from pathlib import Path

import cv2
import torch
import torch.backends.cudnn as cudnn
from PIL import Image
from flask import Flask, render_template, Response, request

os.chdir('./yolov5')
from yolov5.predict import detect
from yolov5.utils.general import check_img_size, non_max_suppression, scale_coords
from yolov5.utils.torch_utils import select_device, time_synchronized
from yolov5.utils.datasets import LoadStreams, LoadImages, LoadWebcam
os.chdir('../')
from deep_sort.src.deep_sort import DeepSort
from deep_sort.utils.parser import get_config
from src.trackableobject import TrackableObject
from src.utils import bbox_rel, compute_color_for_labels, draw_boxes, linear_equation

app = Flask(__name__)

def tracker(ip_address):
    """
    Main tracker function that takes in ip address of camera (0 for webcam)
    and yields annotated frames of captured video
    """
    #arguments
    out = "inference/images"
    source = ip_address
    imgsz = 640
    save_txt = True
    weights = 'yolov5/data/06_models/yolov5m.pt'
    view_img = True
    config_deepsort = "deep_sort/configs/deep_sort.yaml"
    conf_thres = 0.3
    iou_thres = 0.5
    classes = []
    agnostic_nms = True
    augment = True

    webcam = True
    
    # instantiate deepsort
    cfg = get_config()
    cfg.merge_from_file(config_deepsort)
    deepsort = DeepSort(cfg.DEEPSORT.REID_CKPT,
                        max_dist=cfg.DEEPSORT.MAX_DIST, min_confidence=cfg.DEEPSORT.MIN_CONFIDENCE,
                        nms_max_overlap=cfg.DEEPSORT.NMS_MAX_OVERLAP, max_iou_distance=cfg.DEEPSORT.MAX_IOU_DISTANCE,
                        max_age=cfg.DEEPSORT.MAX_AGE, n_init=cfg.DEEPSORT.N_INIT, nn_budget=cfg.DEEPSORT.NN_BUDGET,
                        use_cuda=True)

    # check devices
    device = select_device("")
    if os.path.exists(out):
        shutil.rmtree(out)  # delete output folder
    os.makedirs(out)  # make new output folder
    half = device.type != 'cpu'

    # load model
    model = torch.load(weights, map_location=device)[
        'model'].float()  # load to FP32
    model.to(device).eval()
    if half:
        model.half()  # to FP16

    # data source
    vid_path, vid_writer = None, None
    view_img = True
    cudnn.benchmark = True  # set True to speed up constant image size inference
    dataset = LoadStreams(source, img_size=imgsz)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    
    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
    # run once
    _ = model(img.half() if half else img) if device.type != 'cpu' else None

    save_path = str(Path(out))
    txt_path = str(Path(out)) + '/results.txt'

    #object counting
    trackableObjects = {}
    totalDown = 0
    totalUp = 0

    #iterate through frames in video
    for frame_idx, (path, img, im0s, vid_cap) in enumerate(dataset):
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=augment)[0]

        # Apply NMS
        pred = non_max_suppression(
            pred, conf_thres, iou_thres, classes=classes, agnostic=agnostic_nms)
        t2 = time_synchronized()

        # line start and end
        if webcam:
            H, W = im0s[0].shape[:2]
        else:
            H, W = im0s.shape[:2]
        line_start = (round(W // 2), round(H))
        line_end = (round(W // 2), 0)

        #draw line
        draw_start = line_start[0], H-line_start[1]
        draw_end = line_end[0], H-line_end[1]
        if not webcam:
            cv2.line(im0s, draw_start, draw_end, (0, 255, 255), 2)
        
        # Process detections
        for i, det in enumerate(pred):  # detections per image
            if webcam:  # batch_size >= 1
                p, s, im0 = path[i], '%g: ' % i, im0s[i].copy()
            else:
                p, s, im0 = path, '', im0s

            H, W = im0.shape[:2]

            info = [
                    ("Direction2", totalUp),
                    ("Direction1", totalDown),
                    ]
            # loop over the info tuples and draw them on our frame
            for (i, (k, v)) in enumerate(info):
                text = "{}: {}".format(k, v)
                cv2.putText(im0, text, (10, H - ((i * 20) + 20)),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

            s += '%gx%g ' % img.shape[2:]  # print string
            save_path = str(Path(out) / Path(p).name)

            if det is not None and len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(
                    img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += '%g %ss, ' % (n, names[int(c)])  # add to string

                bbox_xywh = []
                confs = []

                # Adapt detections to deep sort input format
                for *xyxy, conf, cls in det:
                    x_c, y_c, bbox_w, bbox_h = bbox_rel(*xyxy)
                    obj = [x_c, y_c, bbox_w, bbox_h]
                    bbox_xywh.append(obj)
                    confs.append([conf.item()])
                
                #convert to tensor
                xywhs = torch.Tensor(bbox_xywh)
                confss = torch.Tensor(confs)

                # Pass detections to deepsort
                outputs = deepsort.update(xywhs, confss, im0)

                # draw boxes for visualization
                if len(outputs) > 0:
                    #xyxy bounding box and numerical identities
                    bbox_xyxy = outputs[:, :4]
                    identities = outputs[:, -1]
                    # draw_boxes(im0, bbox_xyxy, identities)
                    bbox_xyxy = bbox_xyxy.tolist()
                    
                    for count, person in enumerate(bbox_xyxy):
                        #calculate centroid
                        centroidX = person[0] + ((person[2] - person[0])//2)
                        centroidY = person[3] + ((person[1] - person[3])//2)
                        centroid = [centroidX, centroidY]
                        
                        #id
                        objectID = identities[count]

                        #counter
                        to = trackableObjects.get(objectID, None)

                        direction = 0

                        #linear equation variables
                        gradient, y_incpt = linear_equation(line_start, line_end)

                        if to is None:
                            to = TrackableObject(objectID, centroid)

                        #determining directionality
                        else:
                            if gradient == "horizontal":
                                y = [c[1] for c in to.centroids]
                                direction = centroid[1] - np.mean(y)
                            elif gradient == "vertical":
                                x = [c[0] for c in to.centroids]
                                direction = centroid[0] - np.mean(x)
                            elif abs(gradient) > 1:
                                x = [c[0] for c in to.centroids]
                                direction = centroid[0] - np.mean(x)
                            elif abs(gradient) <= 1:
                                y = [c[1] for c in to.centroids]
                                direction = centroid[1] - np.mean(y)
                            to.centroids.append(centroid)

                        #counting criteria
                        #for horizontal line
                        if gradient == "horizontal":
                            y_incpt = round(y_incpt)
                            if not to.counted and centroid[1] in range(y_incpt-10, y_incpt+11):
                                if direction < 0:
                                    totalUp += 1
                                    to.counted = True
                                elif direction > 0:
                                    totalDown += 1
                                    to.counted = True
                        #for vertical line
                        elif gradient == "vertical":
                            line_starter = round(line_start[0])
                            if not to.counted and centroid[0] in range(line_starter-10, line_starter+11):
                                if direction < 0:
                                    totalUp += 1
                                    to.counted = True
                                elif direction > 0:
                                    totalDown += 1
                                    to.counted = True
                        #for diagonals
                        elif type(gradient) == float:
                            #closer to verticality
                            if abs(gradient) > 1:
                                equation_x = round((H-centroid[1]-y_incpt)/gradient)
                                if not to.counted and (centroid[0] in range(equation_x-10, equation_x+11) 
                                and centroid[1] in range(min(line_start[1], line_end[1]), max(line_start[1], line_end[1])+1)):
                                    if direction < 0:
                                        totalUp += 1
                                        to.counted = True
                                    elif direction > 0:
                                        totalDown += 1
                                        to.counted = True
                            #closer to horizontal
                            if abs(gradient) <= 1:
                                equation_y = H-round((centroid[0]*gradient) + y_incpt)
                                # equation_x = round((centroid[1]-y_incpt)/gradient)
                                if not to.counted and (centroid[1] in range(equation_y-10, equation_y+11) 
                                and centroid[0] in range(min(line_start[0], line_end[0]), max(line_start[0], line_end[0])+1)):
                                    if direction < 0:
                                        totalUp += 1
                                        to.counted = True
                                    elif direction > 0:
                                        totalDown += 1
                                        to.counted = True
                                        
                        # store the trackable object in our dictionary
                        trackableObjects[objectID] = to

                        #draw frame
                        text = "ID {}".format(objectID)
                        cv2.putText(im0, text, (centroid[0] - 10, centroid[1] - 10),
                            cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 255, 0), 1)
                        cv2.circle(im0, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
                        if webcam:
                            cv2.line(im0, draw_start, draw_end, (0, 255, 255), 2)

            else:
                deepsort.increment_ages()

            ret, buffer = cv2.imencode('.jpg', im0)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route('/', methods=["GET", "POST"])
def index():
    """
    Generates flask app that displays index.html and video stream
    """
    if request.method == "POST":
        req = request.form["rtsp_ip"]
        return Response(tracker(req), mimetype='multipart/x-mixed-replace; boundary=frame')
    return render_template('index.html')

# @app.route('/video_feed')
# def video_feed():
#     return Response(tracker(), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == "__main__":
    app.run()