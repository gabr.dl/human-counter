import fileinput
import os
import sys
import shutil

mapper = {
    'people': 0,
    'wheelchair': 1,
    'cone': 2,
    'stroller': 3,
    'trolley': 4,
    'largebin': 5,
    'questand': 6,
    'delivery': 7,
    }

categories = mapper.keys()



def bound_labels(txt_folder):
    """ 
    Bound labels to wihin 1, and prints txt files in doublecheck and 
    triplecheck if initial values are above 1.0 or 1.1 respectively 
    """
    doublecheck = []
    triplecheck = []

    for txt_name in os.listdir(txt_folder):
        txt_path = os.path.join(txt_folder, txt_name)
        # print(txt_path)
        
        # for line in open(txt_path):
        for line in fileinput.input(txt_path, inplace=1):
            a, b, c, d, e = list(map(float, line.split()))

            if max(b, c, d, e) > 1.000:
                doublecheck.append(txt_name)
                if max(b, c, d, e) > 1.100:
                    triplecheck.append(txt_name)

                new_line = "%d %.5f %.5f %.5f %.5f" % (
                    a,
                    min(b, 1.0),
                    min(c, 1.0),
                    min(d, 1.0),
                    min(e, 1.0),
                )
                line = line.replace(line, new_line)

            sys.stdout.write(line)
        fileinput.close()

    print("Please double-check the following labels: \n", doublecheck)
    print("Please triple-check the following labels: \n", triplecheck)





def change_class(old_txt_folder, new_txt_folder, converter):
    """
    For each txt file in {old_txt_folder}, replace id according to converter
    """
    if not os.path.exists(new_txt_folder):
        os.makedirs(new_txt_folder)
    
    for entry in sorted(os.listdir(old_txt_folder)):
        old_txt_path = os.path.join(old_txt_folder, entry)
        new_txt_path = os.path.join(new_txt_folder, entry)
        
        with open(old_txt_path, 'r') as f:
            old_annot = f.readlines() 
        f.close()
        
        with open(new_txt_path, 'w') as f: 
            for line in [entry for entry in old_annot]:
                old_id = line.split()[0]
                new_id = str(converter[int(float(old_id))])
                newline = ' '.join([new_id] + line.split()[1:])
                f.write(newline+'\n')
        f.close()
    # shutil.rmtree(old_txt_folder)



def move_txt(img_folder, txt_scr, txt_dest):
    """ 
    Copies txt labels from {txt_scr} into {txt_dest}, for images in {img_folder}
    """
    img_list = sorted(os.listdir(img_folder))
    
    for img_name in img_list:
        txt_name = img_name.replace('.jpg','.txt')
        file_src = os.path.join(txt_scr, txt_name)
        file_dest = os.path.join(txt_dest, txt_name)
        shutil.copy(file_src, file_dest)





def get_yolo_txt(img_folder, txt_folder, class_id):
    """
    refer to [su2/objdet/S0_get_txt.py]
    """
    from A1_detect import detect
    import cv2
    import numpy as np
    import matplotlib.pyplot as plt
    
    if not os.path.exists(txt_folder):
        os.mkdir(txt_folder)
    
    for img_name in os.listdir(img_folder):
        img_path = os.path.join(img_folder, img_name)
        
        imgarr = cv2.imread(img_path)[:,:,::-1]
        
        pred_arr, marked_img = detect(imgarr, 
                                      score_th=0.30, 
                                      nms_iou=0.30, 
                                      save_img=False, 
                                      maxFeatures=50,
                                      display=False,
                                      humanonly=True, # Only keep rows where id=0 (ie. human)
                                      )
        
        txt_path = os.path.join(txt_folder, img_name.replace('.jpg','.txt'))
        
        N = len(pred_arr)
        
        if N > 0:
            holder = np.hstack([class_id * np.ones((N,1)), 
                                pred_arr[:,:-1]     # take only xc, yc, w, h
                                ])
        else:
            holder = np.array([])
        
        np.savetxt(txt_path, holder,
                   fmt='%.6f', delimiter=' ', newline='\n', header='')   # save prediction array
    
    print("Completed generating txt labels for %s" % img_folder)
    print("Annotation txt files are saved in %s" % txt_folder)



def combine_two_txt(txt_folderA, txt_folderB, txt_folderMerged):
    """
    Args:
        txt_folderA: labels for wheelchair only (all id=1)
        txt_folderB: labels for people only (all id=0)
    Concatenates the contents into single txt (no changes to id)
    """
    import shutil
    
    if not os.path.exists(txt_folderMerged):
        os.mkdir(txt_folderMerged)
    
    txt_listA = sorted([os.path.join(txt_folderA, name) for name in os.listdir(txt_folderA) if name.endswith('.txt')])
    txt_listB = sorted([os.path.join(txt_folderB, name) for name in os.listdir(txt_folderB) if name.endswith('.txt')])
    
    for txt_pathA, txt_pathB in zip(txt_listA, txt_listB):
        name = txt_pathA.split('/')[-1]
        assert name == txt_pathB.split('/')[-1], "check %s vs. %s" % (txt_pathA, txt_pathB)
        
        output_txt = os.path.join(txt_folderMerged, name)
        
        with open(output_txt,'wb') as wfd:
            for file in [txt_pathA, txt_pathB]:
                with open(file,'rb') as fd:
                    shutil.copyfileobj(fd, wfd)
    
    # shutil.rmtree(txt_folderB)
    return None



def remove_excess(img_folder, txt_folder):
    # If txt file does not have corresponding jpg in img_folder, delete it
    set_of_files = set([x.split('.')[0] for x in os.listdir(img_folder)])
    
    removed = 0
    for txt_name in os.listdir(txt_folder):
        if txt_name.split('.')[0] not in set_of_files:
            txt_path = os.path.join(txt_folder, txt_name)
            os.remove(txt_path)
            removed += 1
    print("Deleted %d files from %s" % (removed, txt_folder))
    
        
    
    
def keep_only_common(folder_to_trim, reference_folder):
    """
    Files in `folder_to_trim` will be deleted if not in `reference_folder`
    """
    to_keep = set([x for x in os.listdir(reference_folder)])
    
    for name in os.listdir(folder_to_trim):
        if name not in to_keep:
            filePath = os.path.join(folder_to_trim, name)
            os.remove(filePath)

    




if __name__ == "__main__":

    category = "stroller"
    root = "/home/james/Desktop/desk_holder/su2_all_class/%s" % category
    
    split = "train"
    img_folder = root + "/%s/images" % split
    txt_scr = root + "/labels"
    txt_dest = root + "/%s/labels" % split
    
    # For changing individual folders
    converter = {#0: 1,     # old_id 0 mapes to new_id 1 (wheelchair)
                 #0: 2,                        new_id 2 (cone)
                 0: mapper[category]
                 }
    

    # move_txt(img_folder, txt_scr, txt_dest)
    
    objname = root.split("/")[-1]    #eg. "stroller"
    
    bound_labels(txt_folder=txt_dest)
    
    change_class(old_txt_folder=txt_dest, 
                 new_txt_folder=txt_dest+"_changed", 
                 #new_id=str(mapper[objname])
                 converter=converter
                 )
    
    
    """
    # root = "/home/james/Desktop/desk_holder/su2_all_class/wheelchair"
    txt_output = root + "/%s/labels_human_only" % split
    
    get_yolo_txt(img_folder, 
                 txt_folder=txt_output, 
                 class_id=0)
    
    txt_folderMerged = root + "/%s/labels_merged" % split
    txt_wc_only      = root + "/%s/labels_changed" % split   # ensure that labels changed from 0 to 1 (map id to new 8-class-model)
    txt_people_only  = txt_output                            # contains bbox for human (id=0)
    
    combine_two_txt(txt_folderA = txt_wc_only, 
                    txt_folderB = txt_people_only, 
                    txt_folderMerged = txt_folderMerged)
    """
    