$(document).ready(function()
{
  /**
   * @param items		1ページあたりの最大表示数（省略時の初期値は 5 ）
   * @param contents	タブコンテンツのクラス名（省略時の初期値は 'contents' ）
   * @param time		ページ切り替え時のフェードイン時間（省略時の初期値は 800 ）
   * @param previous	前のページに1つ戻るナビゲーションのテキスト（省略時の初期値は 'Previous&raquo;' ）
   * @param next		次のページに1つ進むナビゲーションのテキスト（省略時の初期値は '&laquo;Next' ）
   * @param start		初期ロード時のタブ開始位置（省略時の初期値は 1 ）
   * @param position	ページナビゲーションの表示位置（ 'top' または 'bottom' を指定可能で省略時の初期値は 'bottom' ）
   * @param scroll	スクロール位置を保持（ true または false を指定可能で省略時の初期値は true ）
   */
  $("#tab").tabpager({
    items: 5,
    contents: 'contents',
    //time: 300,
    previous: '&laquo;前へ',
    next: '次へ&raquo;',
    //start: 1,
    position: 'top',
    //scroll: true
  });
});