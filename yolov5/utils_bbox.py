import colorsys
import random

import cv2
import numpy as np



def draw_on_image(image, pred_arr, class_mapper):
    """
    Add predicted bounding boxes to image
    
    Args
    ----
    image (np.array): RGB image as numpy array
    pred_arr (np.array): Predictions correspond to xc, yc, w, h, score, class_id
    class_mapper (dict): dictionary where key is class_id (as int) and value is obj_name
    """
    H, W = image.shape[:2]
    
    for x, y, w, h, s, cid in pred_arr:
        label = class_mapper[cid]   # cid is an integer corresponding to class-id
        color = (255,0,0) if cid==0 else (0,0,255)
        tl = int(round(0.001 * max(H,W)))  # line thickness
        c1, c2 = (int(W*(x-0.5*w)), int(H*(y-0.5*h))), (int(W*(x+0.5*w)), int(H*(y+0.5*h)))
        cv2.rectangle(image, c1, c2, color, thickness=tl)
        
        tf = max(tl - 2, 1)  # font thickness
        s_size = cv2.getTextSize(str('{:.0%}'.format(s)),0, fontScale=float(tl) / 3, thickness=tf)[0]
        t_size = cv2.getTextSize(label, 0, fontScale=float(tl) / 3, thickness=tf)[0]
        c2 = c1[0] + t_size[0]+s_size[0]+15, c1[1] - t_size[1] -3
        cv2.rectangle(image, c1, c2 , color, -1)  # filled
        cv2.putText(image, '{}: {:.0%}'.format(label, s), (c1[0],c1[1] - 2), 0, float(tl) / 3, [0, 0, 0], thickness=tf, lineType=cv2.FONT_HERSHEY_SIMPLEX)

    return image


def read_txt_into_array(label_path, H, W):
    """Takes annotation txt file in YOLO format and convert x1,y1,x2,y2 for subsequent computation of IOU. 
    
    Args
    ----
    label_path (str): path to txt file containing N lines (for N objects of interest); each line is class_id, xc_norm, yc_norm, w_norm, h_norm
    H (int): image height
    W (int): image width
    
    Return
    ------
    gt_bboxes (np.array): Shape (N, 5); each line is x1, y1, x2, y2, class_id
    """
    with open(label_path, 'r') as f:
        txt   = f.readlines()
        array = np.array([list(map(lambda x: float(x), box.split())) for box in txt]) 
        
    gt_bboxes = np.zeros_like(array)
    if len(gt_bboxes)>0:
        gt_bboxes[:,0] = W * (array[:,1] - 0.5*array[:,3])     # xmin
        gt_bboxes[:,1] = H * (array[:,2] - 0.5*array[:,4])     # ymin
        gt_bboxes[:,2] = W * (array[:,1] + 0.5*array[:,3])     # xmax
        gt_bboxes[:,3] = H * (array[:,2] + 0.5*array[:,4])     # ymax
        gt_bboxes[:,4] = array[:,0]
    
    return gt_bboxes



def read_txt_into_array_w_filter(label_path, H, W, h_th, w_th):
    """Takes annotation txt file in YOLO format and convert x1,y1,x2,y2 for 
    subsequent computation of IOU. Small objects below threshold are ignored.
    
    Args
    ----
    label_path (str): path to txt file containing N lines (for N objects of interest); each line is class_id, xc_norm, yc_norm, w_norm, h_norm
    H (int): image height
    W (int): image width
    h_th (float): min height of obj (relative to H), below which it will be ignored
    w_th (float): min width of obj (relative to W), below which it will be ignored
    
    Return
    ------
    gt_bboxes (np.array): Shape (N, 5) each line is x1, y1, x2, y2, class_id
    """
    with open(label_path, 'r') as f:
        txt   = f.readlines()
        array = np.array([list(map(lambda x: float(x), box.split())) for box in txt]) 
    
    gt_bboxes = []
    
    if len(array)>0:
        w_ok = (array[:,3] >= w_th)
        h_ok = (array[:,4] >= h_th)
        
        array = array[((w_ok==True) & (h_ok==True))]
        
        if len(array)>0:
            gt_bboxes = np.zeros_like(array)
            gt_bboxes[:,0] = W * (array[:,1] - 0.5*array[:,3])     # xmin
            gt_bboxes[:,1] = H * (array[:,2] - 0.5*array[:,4])     # ymin
            gt_bboxes[:,2] = W * (array[:,1] + 0.5*array[:,3])     # xmax
            gt_bboxes[:,3] = H * (array[:,2] + 0.5*array[:,4])     # ymax
            gt_bboxes[:,4] = array[:,0]
    
    return gt_bboxes


def x1y1wh_into_xyxy(box):
    """Convert format of bbox description
    
    Args
    ----
    box (np.array): Shape (N,4) where columns are (x1, y1, w, h)
    
    Return
    ------
    new_box (np.array): Shape (N,4) where columns are (x1, y1, X2, y2)
    """
    if len(box.shape)==1:
        box = box.reshape(1,-1)    # convert shape (4,) into (1,4)
    
    new_box = np.zeros_like(box)
    
    new_box[:,0], new_box[:,1] = box[:,0], box[:,1]
    new_box[:,2], new_box[:,3] = (box[:,0]+box[:,2]), (box[:,1]+box[:,3])
    
    return new_box


def xcycwhnorm_into_xyxy(box, H, W):
    """
    Convert format of bbox description
    
    Args
    ----
    box (np.array): Shape (N,5) where columns are (xc_norm, yc_norm, w_norm, h_norm, score)
    H (int): image height
    W (int): image width
    
    Return
    ------
    new_box (np.array): Shape (N,6) where columns are (x1, y1, X2, y2, score, human_id)
    """
    if len(box)==0:
        # return np.zeros((1,6))
        return np.array([])
    
    else:
        if len(box.shape)==1:
            box = box.reshape(1,-1)    # convert shape (5,) into (1,5)
            
        N = len(box)
        new_box = np.zeros((N,6))
        
        # Caution: Different from read_txt_into_array which has class_id
        if len(box)>0:
            new_box[:,0] = W * (box[:,0] - 0.5*box[:,2])     # xmin
            new_box[:,1] = H * (box[:,1] - 0.5*box[:,3])     # ymin
            new_box[:,2] = W * (box[:,0] + 0.5*box[:,2])     # xmax
            new_box[:,3] = H * (box[:,1] + 0.5*box[:,3])     # ymax
            new_box[:,4] = box[:,4]
            # new_box[:,5] = 0                               # where human_id is 0
        
        return new_box


def bboxes_iou(boxes1, boxes2):
    """Compute intersection-over-union between two boxes
    
    Args
    ----
    boxes1 (np.array): Shape (4,)
    boxes2 (np.array): Shape (N,4) where columns are (x1, y1, x2, y2)
    """
    boxes1_area = (boxes1[..., 2] - boxes1[..., 0]) * (boxes1[..., 3] - boxes1[..., 1])
    boxes2_area = (boxes2[..., 2] - boxes2[..., 0]) * (boxes2[..., 3] - boxes2[..., 1])
    
    left_up       = np.maximum(boxes1[..., 0:2], boxes2[..., 0:2])
    right_down    = np.minimum(boxes1[..., 2:4], boxes2[..., 2:4])
    
    inter_section = np.maximum(right_down - left_up, 0.0)
    inter_area    = inter_section[..., 0] * inter_section[..., 1]
    union_area    = boxes1_area + boxes2_area - inter_area
    ious          = np.maximum(1.0 * inter_area / union_area, np.finfo(np.float32).eps)
    
    return ious


def bboxes_iop(label, pred):
    """
    Compute intersection-over-prediction (denominator is not union)
    
    Args
    ----
    label (np.array): Shape (4,)
    pred (np.array):  Shape (N,4) where columns are (x1, y1, x2, y2)
    """
    pred_area = (pred[..., 2] - pred[..., 0]) * (pred[..., 3] - pred[..., 1])
    
    left_up       = np.maximum(label[..., 0:2], pred[..., 0:2])
    right_down    = np.minimum(label[..., 2:4], pred[..., 2:4])
    
    inter_section = np.maximum(right_down - left_up, 0.0)
    inter_area    = inter_section[..., 0] * inter_section[..., 1]
    iops          = np.maximum(1.0 * inter_area / pred_area, np.finfo(np.float32).eps)
    
    return iops


def evaluate_bboxes(gt_bboxes, pred_array, iou_th, iop_th=0.90):
    """Compute number of TP/FP/FN per object class in current image (here, for SINGLE class only)
    Consider correct (TP) if ground-truth box has IoU>0.5 with one or more prediction box of the correct class or, IoP>0.90
    
    Args
    ----
    gt_bboxes  (np.array): Shape (N_true, 4) where columns are x1,y1,x2,y2
    pred_array (np.array): Shape (N_pred, 5)                   x1,y1,x2,y2,_
    """
    # local only, will transfer data to overall_info and be over-written
    class_id = 0
    info     = {0: {'TP': 0, 'FP': 0, 'FN': 0},    # human
                'num_correct': 0,                  # pred 0 & truth 0;    or  pred>=1 & truth>=1
                'num_wrong':   0,                  # pred 0 but truth>=1; or  pred>=1 but truth 0
                }
    
    gt_one_type   = gt_bboxes
    pred_one_type = pred_array
    truth_found   = []              # initialise empty list, will be over-written if there is 1 or more TP
    
    if len(gt_one_type)==0 and len(pred_one_type)==0:     # no objects and no predictions
        info['num_correct'] += 1
        return info, None, None   #continue
    
    elif len(gt_one_type)==0 and len(pred_one_type)>0:    # all predictions are false positive
        info[class_id]['FP'] += len(pred_one_type)
        info['num_wrong']    += 1
    
    elif len(gt_one_type)>0 and len(pred_one_type)==0:    # all objects are missed
        info[class_id]['FN'] += len(gt_one_type)
        info['num_wrong']    += 1
    
    else:
        used_prediction = set()     # holder to place row_id of predicted box with IOU>0.5
        truth_found     = set()     # holder to place row_id of true box which has at least one matching prediction
        for i, true_object in enumerate(gt_one_type):
            iou_wrt_one_true = bboxes_iou(true_object[:4], pred_one_type[:, :4])
            iop_wrt_one_true = bboxes_iop(true_object[:4], pred_one_type[:, :4])
            
            iou_mask = iou_wrt_one_true > iou_th
            iop_mask = iop_wrt_one_true > iop_th
            for used_id in np.where(iou_wrt_one_true > iou_th)[0]:
                used_prediction.add(used_id)
            for used_id in np.where(iop_wrt_one_true > iop_th)[0]:
                used_prediction.add(used_id)
            if (np.sum(iou_mask)+np.sum(iop_mask)) >= 1:
                truth_found.add(i)
        
        unused_predictions = set(np.arange(len(pred_one_type))) - used_prediction
        
        info[class_id]['TP'] += len(truth_found)
        info[class_id]['FP'] += len(unused_predictions)
        info[class_id]['FN'] += len(gt_one_type) - len(truth_found)    # ie. missed
        info['num_correct'] += 1
    
    return info, truth_found, gt_one_type


def evaluate_bboxes_multi(gt_bboxes, pred_array, info, iou_th=0.50):
    """
    Compute number of TP/FP/FN per object class in current image
    Consider correct (TP) if the ground-truth box has iou>0.5 with one or more prediction box of the correct class
    """
    num_classes = len(info)
    
    if len(gt_bboxes) == 0:
        # print("True empty image; Only has false positives")
        if len(pred_array) == 0:
            return None #continue
        #total_FP += len(pred_array)
        for row in pred_array:
            class_id = int(row[-1])
            info[class_id]['FP'] += 1
        
    elif len(pred_array) == 0:
        if len(gt_bboxes) == 0:
            return None #continue
        for row in gt_bboxes:
            class_id = int(row[-1])
            info[class_id]['FN'] += 1
    
    else:
        for class_id in range(num_classes):
            gt_one_type   = gt_bboxes[gt_bboxes[:,-1]==class_id]
            pred_one_type = pred_array[pred_array[:,-1]==class_id]
            
            class_TP, class_FP, class_FN = 0 ,0, 0     # Will be over-written if non-zero
            
            if len(gt_one_type)==0 and len(pred_one_type)==0:     # no objects and no predictions
                continue
            
            elif len(gt_one_type)==0 and len(pred_one_type)>0:    # all predictions are false positive
                class_FP = len(pred_one_type)
                
            elif len(gt_one_type)>0 and len(pred_one_type)==0:    # all objects are missed
                class_FN = len(gt_one_type)
                
            else:
                used_prediction = set()     # holder to place row_id of predicted box with IOU>0.5
                truth_found     = set()     # holder to place row_id of true box which has at least one matching prediction
                for i, true_object in enumerate(gt_one_type):
                    # iou_wrt_one_true = utils.bboxes_iou(true_object[:4], pred_one_type[:, :4])
                    iou_wrt_one_true = bboxes_iou(true_object[:4], pred_one_type[:, :4])
                    
                    mask = iou_wrt_one_true > iou_th
                    for used_id in np.where(iou_wrt_one_true > iou_th)[0]:
                        used_prediction.add(used_id)
                    if np.sum(mask) >= 1:
                        truth_found.add(i)
                
                unused_predictions = set(np.arange(len(pred_one_type))) - used_prediction
                class_TP = len(truth_found)
                class_FP = len(unused_predictions)
                class_FN = len(gt_one_type) - len(truth_found)    # ie. missed
            
            info[class_id]['TP'] += class_TP
            info[class_id]['FP'] += class_FP
            info[class_id]['FN'] += class_FN
    
    # return None
    return info


def nms(bboxes, iou_threshold_dict):
    """Non-max suppression to remove similar boxes
    
    Args
    ----
    bboxes: np.array of shape (n,6), where n are number of candidate bboxes that have valid coordinates and sufficiently high score (ie. obj conf * class_prob)
            6 columns correspond to (xmin, ymin, xmax, ymax, score, index_of_pred_class)
    
    Returns
    -------
    best_bboxes: List of m arrays (reduced from n after NMS), each of shape (6,)
    """
    classes_in_img = list(set(bboxes[:, 5]))
    best_bboxes = []
    
    for class_id in classes_in_img:
        iou_threshold = iou_threshold_dict[int(class_id)]
        
        cls_mask   = (bboxes[:, 5] == class_id)
        cls_bboxes = bboxes[cls_mask]

        while len(cls_bboxes) > 0:
            max_ind = np.argmax(cls_bboxes[:, 4])
            best_bbox = cls_bboxes[max_ind]
            best_bboxes.append(best_bbox)
            cls_bboxes = np.concatenate([cls_bboxes[: max_ind], cls_bboxes[max_ind + 1:]])
            iou = bboxes_iou(best_bbox[np.newaxis, :4], cls_bboxes[:, :4])
            weight = np.ones((len(iou),), dtype=np.float32)

            iou_mask = iou > iou_threshold       # any two boxes which intersect by iou_threshold or more will be treated as the same object
            weight[iou_mask] = 0.0
        
            cls_bboxes[:, 4] = cls_bboxes[:, 4] * weight
            score_mask = cls_bboxes[:, 4] > 0.
            cls_bboxes = cls_bboxes[score_mask]
    
    return np.array(best_bboxes)


def convert_array_to_list(arr):
    """
    Convert bbox info to format suitable for use with `evaluate_bboxes`
    
    Args
    ----
    arr (np.array): shape (m,6) or (m,5)
    
    Return
    ------
    output: list of arrays of shape (6,)
    """
    output = []
    try:
        n_rows, n_cols = arr.shape
        if n_cols==5:
            for row in arr:
                holder = np.ones((6,))
                holder[:4] = row[:4]
                holder[-1] = row[-1]
                output.append(holder)
        elif n_cols==6:
            for row in arr:
                output.append(row)
        else:
            print("Check array for bounding boxes")
            assert False
    
    except:
        assert len(arr)==0
    
    return output


def draw_bbox(image, bboxes, classes, rel_items=np.ones((5,)), show_label=True):
    """Draw predicted bounding boxes on image
    
    Args
    ----
    image (np.array): array of shape (H,W,3)
    bboxes (list): List of m arrays, each of shape (6,), or single array of shape (m,6)   where m is the number of predicted objects
            6 columns are (x1, y1, x2, y2, score, class_id)
    classes (dict): dictionary where keys are class_id and values are obj names as string
    rel_items (np.array): array of 0's or 1's where 0 indicates that class will be ignored
    show_label (bool): If True, include text box for the predicted object class and score on the marked image
    """
    num_classes = len(classes)
    image_h, image_w, _ = image.shape
    hsv_tuples = [(1.0 * x / num_classes, 1., 1.) for x in range(num_classes)]
    colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
    colors = list(map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))   # list of tuples
    
    random.seed(0)
    random.shuffle(colors)
    fontScale = 0.5 * min(image.shape[:2])/416
    
    image = np.ascontiguousarray(image)
    
    for i, bbox in enumerate(bboxes):
        class_ind = int(bbox[5])
        
        coor  = np.array(bbox[:4], dtype=np.int32)        
        score = bbox[4]
        
        if rel_items[class_ind] == 1:           # For example, if rel_items=[1,1,1,0], class_ind=3 will be ignored
            bbox_color = colors[class_ind]
            bbox_thick = int((image_h + image_w) / 300)
            c1, c2 = (coor[0], coor[1]), (coor[2], coor[3])
            cv2.rectangle(image, c1, c2, bbox_color, min(bbox_thick, 20))
            
            if show_label:
                bbox_mess = '%s: %.2f' % (classes[class_ind], score)
                t_size = cv2.getTextSize(bbox_mess, 0, fontScale, thickness=bbox_thick//2)[0]
                cv2.rectangle(image, c1, (c1[0] + t_size[0], c1[1] - t_size[1] - 3), bbox_color, -1)  # filled
                
                cv2.putText(image, bbox_mess, (c1[0], c1[1]), cv2.FONT_HERSHEY_SIMPLEX,
                            fontScale, (0, 0, 0), bbox_thick, lineType=cv2.LINE_AA)
    return image


def read_class_names(class_file_name):
    """Create a dictionary from txt file
    
    Args
    ----
    class_file_name (str): path to txt file comprising N lines for N object names
    
    Return
    ------
    names (dict): keys are class_id and values are obj names as string
    """
    names = {}
    with open(class_file_name, 'r') as data:
        for ID, name in enumerate(data):
            names[ID] = name.strip('\n')
    return names
