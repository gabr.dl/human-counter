"""Utility functions for Flask"""
import base64
import os
from time import time

import cv2
import numpy as np
from flask import request


def from_base64(encodedImage):
    """
    Reads base64 image to opencv compatiable array

    Parameters
    ----------
    encodedImage : str
        base64 image in string format
    
    Returns
    -------
    image array
    """
    npArr = np.fromstring(base64.b64decode(encodedImage), np.uint8)
    imgArr = cv2.imdecode(npArr, cv2.IMREAD_ANYCOLOR)
    return imgArr


def show_imgpath(path):
    """
    Give full path of the image
    """
    # print("files in folder: ", os.listdir(path))
    img = [i for i in os.listdir(path) if i != '.gitkeep'][0]
    full_path = os.path.join(path, img)
    return full_path


def delete_img(path):
    """
    Delete images from static folder if any

    Returns
    -------
    None
    """
    listofImages = os.listdir(path)
    if len(listofImages) != 0:
        for img in listofImages:
            if img != '.gitkeep':
                os.remove(os.path.join(path, img))


def save_img(path):
    """
    Save image uploaded into static folder

    Returns
    -------
    img : str
        full path of .png image
    """
    # unique name to disable cache
    img_name = "img_{}.png".format(int(time()))
    img = os.path.join(path, img_name)
    file = request.files["image_upload"]
    file.save(img)
    return img
