import argparse
import os
import sys
import cv2
import yaml
import shutil
import subprocess
import numpy as np

from predict import load_model, detect
from train import pipeline_train
    

def view_folders_in_corpus(bucket):
    if len(bucket.split(';')) > 1:
        print("Invalid s3 bucket name")
        return None
    cmd = """aws s3 ls {}/;""".format(bucket)
    process = subprocess.Popen(['/bin/bash', '-c', cmd])
    _, _ = process.communicate()


def delete_specific_file(folder, file):
    """
    Args:
        folder (str): can have intermediate `/` (but no leading or trailing)
        file (str): include extension 
    """
    if len(folder.split(';')) > 1 or len(file.split(';')) > 1:
        print("Invalid s3 bucket or object name")
        return None
    
    cmd = """aws s3 rm s3://{}/{};""".format(folder,file)
    process = subprocess.Popen(['/bin/bash', '-c', cmd])
    _, _ = process.communicate()
    status = process.returncode
    
    if status != 0:
        print("Unable to delete %s/%s" % (folder,file))
        sys.exit(1)
    else:
        print("Successfully deleted %s/%s in S3" % (folder,file))
        

def compare_folder_contents(folderA, folderB):
    """
    Check whether contents (ignore extension) in folderA and folderB are identical
    """
    filesA = set([('.').join(file.split('.')[:-1]) for file in os.listdir(folderA)])
    filesB = set([('.').join(file.split('.')[:-1]) for file in os.listdir(folderB)])
    if len(filesA.difference(filesB)) > 0:
        print("Files present only in folderA: ", filesA.difference(filesB))
    if len(filesB.difference(filesA)) > 0:
        print("Files present only in folderB: ", filesB.difference(filesA))


def download_data(folders, bucket=None):
    """
    Args:
        folders: List of strings. Each string is has images/labels folder in s3://vama-sceneuds-images/SU21corpus
    """
    if bucket is None:
        bucket = "vama-sceneuds-images/SU21corpus"
    if len(bucket.split(';')) > 1:
        print("Invalid s3 bucket name")
        return None
    
    for folder_name in folders:
        cmd = """
            aws s3 sync s3://{}/images/{} ./data/01_raw/images/{}/;
            aws s3 sync s3://{}/labels/{} ./data/01_raw/labels/{}/;
            """.format(bucket, folder_name, folder_name, bucket, folder_name, folder_name)
        process = subprocess.Popen(['/bin/bash', '-c', cmd])
        _, _ = process.communicate()
        status = process.returncode
        
        if status != 0:
            print("Unable to sync with AWS for %s/%s" % (bucket, folder_name))
            sys.exit(1)
        else:
            print("Completed syncing with AWS for %s/%s" % (bucket, folder_name))
            compare_folder_contents("./data/01_raw/images/{}".format(folder_name), "./data/01_raw/images/{}".format(folder_name))


def update_config(modeltype, trainfolder, testfolder, class_list):
    """
    Args:
        modeltype (str): s/m/l/x
        trainfolder (str): Name of folder for train set
        testfolder (str): Name of folder for test set
    """
    configpath = "./conf/custom_5%s.yaml" % modeltype
    
    with open(configpath, "r") as f:
        data_loaded = yaml.safe_load(f)
    
    data_loaded["train"] = ("/").join(data_loaded["train"].split("/")[:4]) + "/%s/"%trainfolder
    data_loaded["val"] = ("/").join(data_loaded["val"].split("/")[:4]) + "/%s/"%testfolder
    data_loaded["names"] = class_list
    data_loaded["nc"] = len(class_list)
    
    with open(configpath, 'w') as f:
        yaml.safe_dump(data_loaded, f, default_flow_style=None)


def copy_weights_from_logs(expt_folder_name, new_ckpt_name):
    """
    Args:
        expt_folder_name (str): Name of logs folder where new training is saved to
        new_ckpt_name (str): Name of give to ckpt file transferred to Model folder
    """
    if new_ckpt_name.split('.')[-1]!="pt":
        new_ckpt_name = new_ckpt_name+".pt"    # append extension
    
    shutil.copy("./logs/{}/weights/best.pt".format(expt_folder_name), "./data/06_models/{}".format(new_ckpt_name))
    


def xyxysc_into_xcycwhnorm(pred_arr, H, W):
    """
    Args:
        pred_arr (np.array): Shape (N,6) where columns are (x1, y1, X2, y2, score, class_id)
        H (int): image height
        W (int): image width
    Return
        yolo_box (np.array): Shape (N,5) where columns are (xc_norm, yc_norm, w_norm, h_norm, score)
    """
    if len(pred_arr)==0:
        return np.array([])
    
    else:
        N = len(pred_arr)
        new_box = np.zeros((N,5))
        arr = pred_arr[np.argsort(pred_arr[:,-1]), :]
        
        new_box[:,0] = arr[:,-1]
        new_box[:,1] = (arr[:,2] + arr[:,0])/(2*W)     # xc_norm
        new_box[:,2] = (arr[:,3] + arr[:,1])/(2*H)     # yc_norm
        new_box[:,3] = (arr[:,2] - arr[:,0])/W         # w_norm
        new_box[:,4] = (arr[:,3] - arr[:,1])/H         # h_norm        
        return new_box


def yolo_txt_from_pred(img_folder, txt_folder, weights_path, num_classes, id_to_keep, min_conf):
    """
    To save predictions of YOLOv5 model as txt files, where columns are class_id, xc_norm, yc_norm, w_norm, h_norm
    Excludes row if the object class id is not within id_to_keep
    Args:
        img_folder (str): Directory of folder to store images for prediction
        txt_folder (str): Directory of folder to store the created txt files
        weights_path (str): saved weights for trained object-detection model
        num_classes (int): Number of possible output classes by model
        id_to_keep (list): class_ids (according to output of model) to keep
        min_conf (float): Minimum score threshold for prediction to be accepted
    """
    if os.path.exists(txt_folder):
        shutil.rmtree(txt_folder)
    os.mkdir(txt_folder)
    
    output_path ="./logs/inference/output"
    model, half, device, _, _ , _ = load_model(num_classes, weights_path, output_path, imgsz=640)
    
    for img_name in os.listdir(img_folder):
        img_path = os.path.join(img_folder, img_name)
        
        imgarr = np.ascontiguousarray(cv2.imread(img_path)[:,:,::-1])
        H, W = imgarr.shape[:2]
        
        # arr, img = detect(imgarr, score_th=min_conf, nms_iou=0.40, w_th=0.03, h_th=0.03, 
        #                   maxFeatures=20, model=model, half=half, device=device)
        arr, img = detect(imgarr, score_th=min_conf, nms_iou=0.40, w_th=0.03, h_th=0.03, 
                          maxFeatures=20, takemodel=model)
        
        txt_path = os.path.join(txt_folder, img_name.replace('.jpg','.txt'))
        
        N = len(arr)
        
        if N > 0:
            # current predictions are in the form x1, y1, x2, y2, score, class_id
            arr = arr[np.in1d(arr[:,-1], id_to_keep)]
            yoloarr = xyxysc_into_xcycwhnorm(arr, H, W)
        else:
            yoloarr = np.array([])
        
        np.savetxt(txt_path, yoloarr,
                   fmt='%.6f', delimiter=' ', newline='\n', header='')   # save prediction array
    
    print("Completed generating txt labels for %s" % img_folder)
    print("Annotation txt files are saved in %s" % txt_folder)
    


def create_merged_train_test(new_name, train_img_folders, test_img_folders, train_txt_folders, test_txt_folders):
    """
    Args:
        new_name (str): Name to assign to folders of new merged dataset
        train_img_folders (str): List of directories comprising training images
        test_img_folders (str): List of directories comprising test images
        train_txt_folders (str): List of directories comprising training labels
        test_txt_folders (str): List of directories comprising test labels
    """
    train_img_merged = "./data/01_raw/images/%s_train" % new_name
    test_img_merged = "./data/01_raw/images/%s_test" % new_name
    train_txt_merged = "./data/01_raw/labels/%s_train" % new_name
    test_txt_merged = "./data/01_raw/labels/%s_test" % new_name
    
    list_of_old = [train_img_folders, test_img_folders, train_txt_folders, test_txt_folders]
    list_of_new = [train_img_merged,test_img_merged,train_txt_merged,test_txt_merged]
    
    
    for old_folder_list, new_folder in zip(list_of_old, list_of_new):
        if os.path.exists(new_folder):
            shutil.rmtree(new_folder)
        os.mkdir(new_folder)
        
        for old_folder in old_folder_list:
            for name in os.listdir(old_folder):
                shutil.move(os.path.join(old_folder,name), new_folder)
            shutil.rmtree(old_folder)
    
    
    
def train_standard_fine(opt, config, num_epochs_standard, num_epochs_fine, 
    train_folder, test_folder, output_name, obj_class_list, ckpt_to_load=None):
    """
    Args:
        config: Dictionary loaded from yaml file
        num_epochs_standard (int): Number of epochs for standard training
        num_epochs_fine (int): Number of epochs for finetuning
        train_folder (str): Name of trainset
        test_folder (str): Name of testset
        output_name (str): Name of output weights
        ckpt_to_load (str): Name of weights to load
    """
    if ckpt_to_load is None:
        ckpt_to_load = "yolov5%s"%config["modeltype"]
    
    update_config(config["modeltype"], train_folder, test_folder, obj_class_list)
    _, exp_name = pipeline_train(
        opt=opt,
        finetune=False, 
        overwrite_epochs=num_epochs_standard, 
        checkpoint_to_resume=ckpt_to_load
    )
    copy_weights_from_logs(expt_folder_name=exp_name, new_ckpt_name=output_name)
    # _, exp_name = pipeline_train(
    #     opt=opt,
    #     finetune=True, 
    #     overwrite_epochs=num_epochs_fine,
    #     checkpoint_to_resume=output_name
    # )
    # copy_weights_from_logs(expt_folder_name=exp_name, new_ckpt_name=output_name)

    
    

def check_unseen_obj(listA, listB, listC):
    """
    Checks for unseen objects in listC (not present in listA or listB)
    Returns True if at least one item in listC is not seen before in the other two lists
    Args:
        listA: List of object names as string, in DatasetA
        listB: List of object names as string, in DatasetB
        listC: List of object names as string, to be in merged Dataset
    """
    setAB = set(listA).union(set(listB))
    for name in listC:
        if name not in setAB:
            print("%s is not found in DatasetA or DatasetB. Please edit merge_annotations.yaml" % name)
            return True
    return False
    
    

def pipeline_to_merge(train_modelA, train_modelB):
    """
    Args:
        config: Dictionary loaded from yaml file
        train_modelA (bool): If True, performs training to label objects in A. Otherwise, using existing weights.
        train_modelB (bool): If True, performs training to label objects in B. Otherwise, using existing weights.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str, default='yolov5m.pt', help='initial weights path')
    parser.add_argument('--cfg', type=str, default='', help='model.yaml path')
    parser.add_argument('--data', type=str, default='conf/custom_5m.yaml', help='data.yaml path')
    parser.add_argument('--hyp', type=str, default='conf/hyp.scratch.yaml', help='hyperparameters path')
    parser.add_argument('--logdir', type=str, default='logs/')
    parser.add_argument('--epochs', type=int, default=30)
    parser.add_argument('--optimizertype', type=str, default="sgdn")
    parser.add_argument('--batch-size', type=int, default=4, help='total batch size for all GPUs')
    parser.add_argument('--img-size', nargs='+', type=int, default=[640, 640], help='[train, test] image sizes')
    parser.add_argument('--rect', action='store_true', help='rectangular training')
    parser.add_argument('--resume', nargs='?', const=True, default=False, help='resume most recent training')
    parser.add_argument('--nosave', action='store_true', help='only save final checkpoint')
    parser.add_argument('--notest', action='store_true', help='only test final epoch')
    parser.add_argument('--noautoanchor', action='store_true', help='disable autoanchor check')
    parser.add_argument('--evolve', action='store_true', help='evolve hyperparameters')
    parser.add_argument('--bucket', type=str, default='', help='gsutil bucket')
    parser.add_argument('--cache-images', action='store_true', help='cache images for faster training')
    parser.add_argument('--image-weights', action='store_true', help='use weighted image selection for training')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--multi-scale', action='store_true', help='vary img-size +/- 50%%')
    parser.add_argument('--single-cls', action='store_true', help='train as single-class dataset')
    parser.add_argument('--adam', action='store_true', help='use torch.optim.Adam() optimizer')
    parser.add_argument('--sync-bn', action='store_true', help='use SyncBatchNorm, only available in DDP mode')
    parser.add_argument('--local_rank', type=int, default=-1, help='DDP parameter, do not modify')
    parser.add_argument('--log-imgs', type=int, default=16, help='number of images for W&B logging, max 100')
    parser.add_argument('--workers', type=int, default=8, help='maximum number of dataloader workers')
    parser.add_argument('--project', default='runs/train', help='save to project/name')
    parser.add_argument('--name', default='exp', help='save to project/name')
    parser.add_argument('--exist-ok', action='store_true', help='existing project/name ok, do not increment')
    opt = parser.parse_args()
    
    repeat=True
    while repeat:
        with open("./conf/merge_annotations.yaml", "r") as f:
            config = yaml.safe_load(f)
        repeat = check_unseen_obj(config['class_listA'], config['class_listB'], config['merged_class_list'])
    
    bucket = config["bucket"]   #"vama-sceneuds-images/SU21corpus"
    merged_class_list = config["merged_class_list"]
    modeltype = config["modeltype"]   # s/m/l/x
    
    trainfolderA = config["trainfolderA"]   #"sample5_train_100"
    testfolderA = config["testfolderA"]   #"sample5_test_25"
    class_listA = config["class_listA"]   #['wheelchair', 'cone', 'stroller', 'trolley', 'largebin']
    obj_idA = config["obj_idA"]
    num_classesA = config["num_classesA"]   #5
    min_confA = config["min_confA"]   #0.50
    weights_pathA = config["weights_pathA"]   #"./data/06_models/best_5class_fz10t80.pt"
    
    trainfolderB = config["trainfolderB"]   #"people_train_40"
    testfolderB = config["testfolderB"]   #"people_test_10"
    class_listB = config["class_listB"]   #['people']
    obj_idB = config["obj_idB"]
    num_classesB = config["num_classesB"]
    min_confB = config["min_confB"]
    weights_pathB = config["weights_pathB"]
    
    folders = [trainfolderA, testfolderA, trainfolderB, testfolderB]
    download_data(folders, bucket)
    
    # Keep only the objects to be carried into new model
    mapper_A = dict([(name,cid) for name, cid in zip(class_listA, obj_idA)])
    mapper_B = dict([(name,cid) for name, cid in zip(class_listB, obj_idB)])    
    mapper_merged = dict([(name,i) for i,name in enumerate(merged_class_list)])
    obj_in_merged = set(mapper_merged.keys())
    
    converterA = {}   # for mapping objects in DatasetA (eg. 5-class) into merged dataset
    converterB = {}   #                        DatasetB (eg. new obj)
    
    for name, cid in mapper_A.items():
        if name in obj_in_merged:
            converterA[cid] = mapper_merged[name]   # to map old_id into new_id
    
    for name, cid in mapper_B.items():
        if name in obj_in_merged:
            converterB[cid] = mapper_merged[name]
    
    
    """
    2(I). Train new one-class model on new DatasetA
    """
    if train_modelB is True:
        train_standard_fine(
            opt=opt,
            config=config, 
            num_epochs_standard=30, 
            num_epochs_fine=20, 
            train_folder=trainfolderB, 
            test_folder=testfolderB, 
            output_name=config["new_weightsB"], 
            obj_class_list=class_listB, 
            ckpt_to_load=weights_pathB
        )
    
    """
    2(II). Finetune existing model (if needed)
    """
    if train_modelA is True:
        train_standard_fine(
            opt=opt,
            config=config, 
            num_epochs_standard=30, 
            num_epochs_fine=20, 
            train_folder=trainfolderA, 
            test_folder=testfolderA, 
            output_name=config["new_weightsA"], 
            obj_class_list=class_listA, 
            ckpt_to_load=weights_pathA
        )
    
    """
    3(I). Use new one-class modelB to generate labels on DatasetA (containing wheelchair, cone, stroller etc.)
    3(II). Use existing multi-class modelA to generate labels on DatasetB (containing new object)
    """
    img_folderA1 = "./data/01_raw/images/{}".format(trainfolderA)
    origin_txtA1 = "./data/01_raw/labels/{}".format(trainfolderA)
    pred_txtA1 = "./data/01_raw/labels/{}_pred".format(trainfolderA)
    changed_txtA1 = "./data/01_raw/labels/{}_pred_changed".format(trainfolderA)
    merged_txtA1 = "./data/01_raw/labels/{}_merged".format(trainfolderA)
    
    ckpt_for_B = "./data/06_models/%s.pt"%config["new_weightsB"] if train_modelB else "./data/06_models/%s.pt"%weights_pathB
    yolo_txt_from_pred(img_folderA1, pred_txtA1, ckpt_for_B, num_classesB, obj_idB, min_confB)
    
    img_folderA2 = "./data/01_raw/images/{}".format(testfolderA)
    origin_txtA2 = "./data/01_raw/labels/{}".format(testfolderA)
    pred_txtA2 = "./data/01_raw/labels/{}_pred".format(testfolderA)
    changed_txtA2 = "./data/01_raw/labels/{}_pred_changed".format(testfolderA)
    merged_txtA2 = "./data/01_raw/labels/{}_merged".format(testfolderA)
    
    yolo_txt_from_pred(img_folderA2, pred_txtA2, ckpt_for_B, num_classesB, obj_idB, min_confB)
    
    # To get predictions of existing object classes (wheelchair, cones, etc.), on new train and test folders
    
    img_folderB1 = "./data/01_raw/images/{}".format(trainfolderB)
    origin_txtB1 = "./data/01_raw/labels/{}".format(trainfolderB)
    pred_txtB1 = "./data/01_raw/labels/{}_pred".format(trainfolderB)
    changed_txtB1 = "./data/01_raw/labels/{}_pred_changed".format(trainfolderB)
    merged_txtB1 = "./data/01_raw/labels/{}_merged".format(trainfolderB)
    
    ckpt_for_A = "./data/06_models/%s.pt"%config["new_weightsA"] if train_modelA else "./data/06_models/%s.pt"%weights_pathA
    yolo_txt_from_pred(img_folderB1, pred_txtB1, ckpt_for_A, num_classesA, obj_idA, min_confA)
    
    img_folderB2 = "./data/01_raw/images/{}".format(testfolderB)
    origin_txtB2 = "./data/01_raw/labels/{}".format(testfolderB)
    pred_txtB2 = "./data/01_raw/labels/{}_pred".format(testfolderB)
    changed_txtB2 = "./data/01_raw/labels/{}_pred_changed".format(testfolderB)
    merged_txtB2 = "./data/01_raw/labels/{}_merged".format(testfolderB)
    
    yolo_txt_from_pred(img_folderB2, pred_txtB2, ckpt_for_A, num_classesA, obj_idA, min_confA)
    
    """
    4(I). For DatasetA, change id of predictions (from modelB) of new object, then append to original labels
    """
    from utils_label import change_class, combine_two_txt
    ## previously from P1_change_copy_label
    
    # Use converterA to map predictions from modelA into new_id in merged dataset, 
    # use converterB to map predictions from modelB into new_id in merged dataset, 
    # then append these two annotations
    if not list(converterB.keys())==list(converterB.values()):
        print("Changing class_id according to converterB")
        change_class(pred_txtA1, changed_txtA1, converterB)
        change_class(pred_txtA2, changed_txtA2, converterB)
    else:
        # copy entire txt folder
        cmd = """
            cp -r {} {};
            cp -r {} {};
            rm -rf {};
            rm -rf {};
            """.format(pred_txtA1, changed_txtA1, pred_txtA2, changed_txtA2, pred_txtA1, pred_txtA2)
        process = subprocess.Popen(['/bin/bash', '-c', cmd])
        _, _ = process.communicate()
    
    combine_two_txt(txt_folderA = origin_txtA1, 
                    txt_folderB = changed_txtA1, 
                    txt_folderMerged = merged_txtA1)   # train folder
    
    combine_two_txt(txt_folderA = origin_txtA2, 
                    txt_folderB = changed_txtA2, 
                    txt_folderMerged = merged_txtA2)   # test folder
    
    """
    4(II). For DatasetB, change id of predictions (eg. 5 classes from modelA), then append to original labels
    """
    if not list(converterA.keys())==list(converterA.values()):
        print("Changing class_id according to converterA")
        change_class(pred_txtB1, changed_txtB1, converterA)
        change_class(pred_txtB2, changed_txtB2, converterA)
    else:
        # copy entire txt folder
        cmd = """
            cp -r {} {};
            cp -r {} {};
            """.format(pred_txtB1, changed_txtB1, pred_txtB2, changed_txtB2)
        process = subprocess.Popen(['/bin/bash', '-c', cmd])
        _, _ = process.communicate()
    
    combine_two_txt(txt_folderA = origin_txtB1, 
                    txt_folderB = changed_txtB1, 
                    txt_folderMerged = merged_txtB1)   # train folder
    
    combine_two_txt(txt_folderA = origin_txtB2, 
                    txt_folderB = changed_txtB2, 
                    txt_folderMerged = merged_txtB2)   # test folder
    
    """
    5(I). Combine DatasetA and DatasetB into single img_folder and single txt_folder. 
          Ensure annotations are for all objects in both datasets
    """
    create_merged_train_test(
        new_name=config["merged_name"],
        train_img_folders=[img_folderA1,img_folderB1],
        test_img_folders=[img_folderA2,img_folderB2],
        train_txt_folders=[merged_txtA1,merged_txtB1],
        test_txt_folders=[merged_txtA2,merged_txtB2],
    )
    
    




if __name__ == "__main__":    
    pipeline_to_merge(train_modelA=True, train_modelB=True)
    