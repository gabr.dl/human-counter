import os
import sys
import time
import numpy as np
from pprint import pprint
import matplotlib.pyplot as plt

import cv2
import imageio

from predict import detect
import src.P0_utils as utils




if __name__ == "__main__":
    start = time.time()

    # img_folder = "./data/01_raw/images/five_test_795"
    # txt_folder = "./data/01_raw/labels/five_test_795"
    img_folder = "./data/01_raw/images/mergedsix_test"
    txt_folder = "./data/01_raw/labels/mergedsix_test"
    
    img_path_list = [os.path.join(img_folder, entry) for entry in os.listdir(img_folder)]

    img_path_list = sorted(img_path_list)

    save_dir = "./logs/pred_output"
    
    display = True

    info = dict([(i, {'TP': 0, 'FP': 0, 'FN': 0}) for i in range(6)])
    TP = 0
    FP = 0
    FN = 0
    num_correct = 0
    num_wrong = 0

    w_th = 0.03
    h_th = 0.03
    
    
    for i, img_path in enumerate(img_path_list):
        print("+++++", img_path)

        name = img_path.split("/")[-1]
        txt_path = img_path.replace("/images/", "/labels/").replace(".jpg", ".txt")

        image = cv2.imread(img_path)[:, :, ::-1]  # convert cv2's BGR into RGB image

        H, W, _ = image.shape

        pred_arr, marked_img = detect(image, 
                                      score_th=0.30, 
                                      nms_iou=0.50)
        
        # pred_boxes = utils.xcycwhnorm_into_xyxy(pred_arr, H, W)
        pred_boxes = pred_arr   # already in xyxy format

        gt_arr = utils.read_txt_into_array_w_filter(txt_path, H, W, h_th, w_th)

        gt_boxes = utils.convert_array_to_list(gt_arr)  # also convert (N.5) into 6 columns
        # gt_boxes = np.append(gt_arr, np.ones((len(gt_arr),1)), axis=1)
        gt_boxes = np.array(gt_boxes)
        
        info = utils.evaluate_bboxes_multi(gt_boxes, pred_boxes, info, iou_th=0.50)
        
    end = time.time()

    print("Total time elapsed: %.1f seconds" % (end - start))
    pprint(info)
    
    try:
        for key, val in info.items():
            """
            if key==0:
                print("Excluding predicitons and labels corresponding to id=0")
                continue
            """
            TP += val['TP']
            FP += val['FP']
            FN += val['FN']
            print("key: ", key)
            print("val: ", TP, FP, FN)

        precision = TP / (TP + FP)
        recall = TP / (TP + FN)
        f1score = 2 * TP / (2 * TP + FP + FN)
        print(
            "precision: %.3f, recall: %.3f, f1score: %.3f"
            % (precision, recall, f1score)
        )
    except:
        print("Unable to compute precision | recall | f1score")
        print("TP: %d, FP: %d, FN: %d" % (TP, FP, FN))
