import json
import os
import traceback
from time import time

import yaml
import cv2
import numpy as np
from flask import Flask, abort, render_template, request
import torch

import utils_serve as utils
from predict import detect, load_model, show_bbox, conver_xyxy_into_yolo, conver_arr_into_json, mapper, class_mapper, opt, imgsz

num_classes = len(mapper)
weights_path = "./data/07_model_output/best.pt" 
model, half, device, imgsz, names_, colors = load_model(num_classes, weights_path, opt, imgsz)


app = Flask(__name__)

image_folder = "static/img"
result_folder = "static/result"
MODULE = "humandetection"
with open("./conf/custom_5m.yaml", "r") as f:
    config = yaml.load(f)

# API --------------
@app.route('/api', methods=["POST"])
def image_request():
    JScontent = request.json
    try:
        # EXTRACT REQUEST CONTENT --------
        # pred_type = JScontent['requests'][0]['features'][0]['type']
        try: min_height = JScontent["requests"][0]["features"][0]["min_height"]
        except: min_height = 0.03
        try: min_width = JScontent['requests'][0]['features'][0]['min_width']
        except: min_width = 0.03
        try: maxResults = JScontent['requests'][0]['features'][0]['maxResults']
        except: maxResults = 20
        try: score_th = JScontent['requests'][0]['features'][0]['score_th']
        except: score_th = 0.3
        try: nms_iou = JScontent['requests'][0]['features'][0]['nms_iou']
        except: nms_iou = 0.4
        encodedImage = JScontent["requests"][0]["image"]["content"]
        # decode image
        decodedImage = utils.from_base64(encodedImage)
        # prediction
        starttime = time()
        # json_output = detect(image, score_th=0.30, nms_iou=0.40)
        pred_arr, _ = detect(decodedImage, score_th=0.30, nms_iou=0.40, maxFeatures=maxResults)
        H, W = decodedImage.shape[:2]
        xyxysc = pred_arr
        marked_img = show_bbox(decodedImage, 
                               xyxysc, 
                               config["names"], 
                               colors, 
                               save=True,
                               display=False)
        output_path = os.path.join(result_folder,"%d.jpg"%int(100*time()%10**6))
        cv2.imwrite(output_path, marked_img[:,:,::-1])
        xcycwhsc = conver_xyxy_into_yolo(pred_arr, H, W)
        json_output = conver_arr_into_json(xcycwhsc, class_mapper)
        endtime = time()
        json_output["time-taken"] = endtime - starttime
        return json_output

    except:
        print(traceback.format_exc())
        abort(500, traceback.format_exc())



# UI INDEX --------------
@app.route('/')
def index():
    return render_template('index.html', module=MODULE)



# UI IMAGE UPLOAD --------------
@app.route('/upload', methods=["POST"])
def upload_file():
    utils.delete_img(result_folder)
    new_img = utils.save_img(image_folder)
    return render_template('index.html', img_show=new_img, \
                            module=MODULE)



# UI FRONTEND JSON OUTPUT --------------
@app.route('/output', methods=["POST"])
def image_algo():
    # show uploaded image path
    img_path = utils.show_imgpath(image_folder)
    # print(os.path.exists(img_path))
    # print("-------- ", img_path)
    

    # convert image to np array
    image = np.ascontiguousarray(cv2.imread(img_path)[:,:,::-1])
    
    starttime = time()
    pred_arr, _ = detect(image, score_th=0.30, nms_iou=0.40)
    H, W = image.shape[:2]
    xyxysc = pred_arr
    marked_img = show_bbox(image, 
                           xyxysc, 
                           config["names"], 
                           colors, 
                           save=True,
                           display=False)
    output_path = os.path.join(result_folder,"%d.jpg"%int(100*time()%10**6))
    cv2.imwrite(output_path, marked_img[:,:,::-1])
    xcycwhsc = conver_xyxy_into_yolo(pred_arr, H, W)
    json_output = conver_arr_into_json(xcycwhsc, class_mapper)
    endtime = time()
    
    runtime = 'Runtime (CPU): {:.2f} sec'.format(endtime-starttime)
    json_output = json.dumps(json_output, indent=2)
    
    # show new image path with prediction bboxes
    utils.delete_img(image_folder)
    img_path = utils.show_imgpath(result_folder)
    return render_template('index.html', json_output=json_output, \
                            img_show=img_path, disabled="disabled", \
                            module=MODULE, runtime=runtime)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)