"""streamlit server for demo site"""

import json
import time
import yaml
import requests
import numpy as np
import streamlit as st
from PIL import Image

from predict import detect, load_model, show_bbox, conver_xyxy_into_yolo, conver_arr_into_json, mapper, class_mapper, opt, imgsz

num_classes = len(mapper)
weights_path = "./data/07_model_output/best.pt" 
model, half, device, imgsz, names_, colors = load_model(num_classes, weights_path, opt, imgsz)

with open("./conf/custom_5m.yaml", "r") as f:
    config = yaml.load(f)

json_data = {
    "requests": [
        {
            "features": [
                {"maxResults": 20,
                 "min_height": 0.03,
                 "min_width": 0.03,
                 "score_th": 0.3,
                 "nms_iou": 0.4,
                 "type": "HUMAN_DETECTION"
                }
            ],
            "image": {"content": None}
        }
    ]
}


def encode_image(image):
    """
    Args:
        image: PIL.PngImagePlugin.PngImageFile
    Returns:
        base64 encoded string
    """
    buffered = BytesIO()
    image.save(buffered, format="png")
    base64_bytes = base64.b64encode(buffered.getvalue())
    return base64_bytes.decode("utf-8")


def decode_image(field):
    """
    Args:
        field: base64 encoded string
    Returns:
        numpy.array
    """
    array = np.frombuffer(base64.b64decode(field), dtype=np.uint8)
    image_array = cv2.imdecode(array, cv2.IMREAD_ANYCOLOR)  # BGR
    return image_array


def hide_navbar():
    """hide navbar so its not apparent this is from streamlit"""
    hide_streamlit_style = """
    <style>
    #MainMenu {visibility: hidden;}
    footer {visibility: hidden;}
    </style>
    """
    st.markdown(hide_streamlit_style, unsafe_allow_html=True) 



def send2api(api, image, json_data,\
             maxfeatures, min_height, min_width, \
             score_th, nms_iou):
    """Sends JSON request & recieve a JSON response
    
    Args
    ----
    api (str): API endpoint
    image (image file): opened image file
    json_data (dict): json request template
    token (str): API token
    maxfeatures (int): max no. of objects to detect in image
    min_height (float): min height of bounding box (relative to H) to be included
    min_width (float): min width of bounding box (relative to W) to be included
    score_th (float): min prediciton score for bounding box to be included
    nms_iou (float): intersection over union, for non-max suppression
    
    Returns
    -------
    json_response (dict): API response
    """
    base64_bytes = encode_image(image)

    # token = {"X-Bedrock-Api-Token": token}
    json_data["requests"][0]["image"]["content"] = base64_bytes
    json_data["requests"][0]["features"][0]["maxResults"] = maxfeatures
    json_data["requests"][0]["features"][0]["min_height"] = min_height
    json_data["requests"][0]["features"][0]["min_width"] = min_width
    json_data["requests"][0]["features"][0]["score_th"] = score_th
    json_data["requests"][0]["features"][0]["nms_iou"] = nms_iou

    # response = requests.post(api, headers=token, json=json_data)
    response = requests.post(api, json=json_data)
    json_response = response.content.decode('utf-8')
    json_response = json.loads(json_response)
    return json_response



def main():
    """design streamlit fronend"""
    # token = st.text_input("API Token")
    uploaded_file = st.file_uploader("Upload an image.")
    
    if uploaded_file is not None:
        image = Image.open(uploaded_file)
        SIZE = 400,400
        image.thumbnail(SIZE, Image.ANTIALIAS)
        # header
        st.title("SU2.1 Object Detection")
        # st.subheader("Uploaded Image")
        # st.image(image, width=400)
        
        # sidebar
        st.sidebar.title("Change Parameters")
        maxfeatures = st.sidebar.slider("Max Features", min_value=1, max_value=50, value=20, step=1)
        min_height = st.sidebar.slider("Min Height", min_value=0.01, max_value=0.05, value=0.03, step=0.01)
        min_width = st.sidebar.slider("Min Width", min_value=0.01, max_value=0.05, value=0.03, step=0.01)
        score_th = st.sidebar.slider("Score Th", min_value=0.1, max_value=0.5, value=0.3, step=0.1)
        nms_iou = st.sidebar.slider("NMS IOU", min_value=0.1, max_value=0.5, value=0.4, step=0.1)

        # if st.button("Send API Request"):
        if True:
            st.title("Results")
    
            # send request
            start_time = time.time()
            
            decodedImage = np.ascontiguousarray(np.array(image)[:,:,:3])
            pred_arr, _ = detect(
                decodedImage, 
                score_th=score_th, 
                nms_iou=nms_iou, 
                w_th=min_width, 
                h_th=min_height,
                maxFeatures=maxfeatures,
            )
            H, W = decodedImage.shape[:2]
            
            marked_img = show_bbox(decodedImage, 
                                   pred_arr, 
                                   config["names"], 
                                   colors, 
                                   save=True,
                                   display=False)
            xcycwhsc = conver_xyxy_into_yolo(pred_arr, H, W)
            json_output = conver_arr_into_json(xcycwhsc, class_mapper)
            
            ### save marked image
            # from datetime import datetime
            # import cv2
            # filename = "/logs/pred_%s.jpg" % datetime.now().strftime('%Y%m%d_%H%M')
            # cv2.imwrite(filename, marked_img[:,:,::-1])
            
            latency = time.time() - start_time
            st.write("**Est. latency (CPU) = `{:.3f} s`**".format(latency))
            
            # result image
            st.subheader("Visualize Output")
            st.image(marked_img, width=400)
            st.subheader("API Response")
            st.json(json.dumps(json_output, indent=2))
            return json_output
            
        
            
if __name__ == "__main__":
    # api = "https://muddy-sunset-8466.pub.dc4ai.bdrk.ai"
    # api = "http://localhost:5001/api"
    hide_navbar()
    main()