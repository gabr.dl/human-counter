
import argparse
import os
import cv2
import json
import yaml
import shutil
import logging
import subprocess
import torch
import torch.distributed as dist
from torch.utils.tensorboard import SummaryWriter
import numpy as np
from pathlib import Path

from bedrock_client.bedrock.api import BedrockApi

import train
import utils_bbox
from utils.general import check_file, get_latest_run, increment_dir, set_logging
from utils.torch_utils import select_device
from utils_s3 import download_weights, download_config
from utils_train import get_file_list, folder_naming_convention, download_data, update_config

logger = logging.getLogger(__name__)


def evaluate(img_folder, txt_folder, w_th=0.03, h_th=0.03, score_th=0.3, nms_iou=0.4):
    """
    Return precision, recall and F1-score
    """
    # load after training complete so new weights are loaded
    from predict import detect

    img_list = get_file_list(img_folder, (".jpg", "jpeg", ".png", "bmp"))
    txt_list = get_file_list(txt_folder, (".txt"))
    
    TP = FP = FN = 0
    info = dict([(i, {'TP': 0, 'FP': 0, 'FN': 0}) for i in range(6)])
    
    print("Performing evaluation")
    print("Validation folder: ", img_folder, txt_folder)
    # print("\n img_list: ", img_list)
    # print("\n txt_list: ", txt_list)
    
    for imgname, txtname in zip(img_list, txt_list):
        img_path = os.path.join(img_folder, imgname)
        txt_path = os.path.join(txt_folder, txtname)
        
        # convert cv2's BGR into RGB image
        image = cv2.imread(img_path)[:, :, ::-1]
        H, W, _ = image.shape

        pred_arr, _ = detect(image,
                             score_th=score_th,
                             nms_iou=nms_iou,
                             w_th=w_th,
                             h_th=h_th)
        
        # pred_boxes = utils.xcycwhnorm_into_xyxy(pred_arr, H, W)
        pred_boxes = pred_arr   # already in xyxy format
        gt_arr = utils_bbox.read_txt_into_array_w_filter(txt_path, H, W, h_th, w_th)
        gt_boxes = utils_bbox.convert_array_to_list(gt_arr)   # also add 6th column
        gt_boxes = np.array(gt_boxes)
        
        info = utils_bbox.evaluate_bboxes_multi(gt_boxes, pred_boxes, info, iou_th=0.50)
        # dictionary will be updated with each pass

    for key, val in info.items():
        TP += val['TP']
        FP += val['FP']
        FN += val['FN']

    FP = max(FP, 1e-3)  # To prevent division by zero
    FN = max(FN, 1e-3)  # To prevent division by zero
    
    print("info: \n", info)
    print("TP: %d, FP: %d, FN: %d" % (TP,FP,FN))
    precision = TP / (TP + FP)
    recall = TP / (TP + FN)
    f1score = 2 * TP / (2 * TP + FP + FN)
    return precision, recall, f1score
    

def log_metrics(precision, recall, f1, results="logs/results.json"):
    """
    Log metrics to bedrock & results.json
    """
    print("Precision: %.3f, Recall: %.3f, F1-score: %.3f" % (precision, recall, f1))

    with open(results, "w+") as f:
        data = json.dumps({"Precision": precision, "Recall": recall, "F1": f1}, indent=2)
        f.write(data)
    
    bedrock = BedrockApi(logging.getLogger(__name__))
    bedrock.log_metric("Precision", precision)
    bedrock.log_metric("Recall", recall)
    bedrock.log_metric("F1", f1)



def copy2bedrock(weights_dir, log_dir):
    """
    copy model and result logs to Bedrock
    """
    bedrock_dir = "/artefact"   # to store both weights and logs
    
    # copy weights
    src = os.path.join(weights_dir, "best.pt")
        
    if os.environ.get("BEDROCK_POD_NAME"): 
        dest = os.path.join(bedrock_dir, "best.pt")
    else:
        dest = os.path.join("./data/07_model_output", "best.pt")
    
    shutil.copy2(src, dest)
    print("Successfully copied weights from %s to %s" % (src, dest))

    # copy logs
    for file_ in os.listdir(log_dir):
        src = os.path.join(log_dir, file_)
        if os.path.isfile(src):
            dest = os.path.join(bedrock_dir, file_)
            shutil.copy2(src, dest)
            

def pipeline_train(opt, finetune, checkpoint_to_resume, overwrite_epochs=None, freeze=False):
    opt.weights = "./data/06_models/%s" % checkpoint_to_resume
    if finetune:
        opt.hyp = "./conf/hyp.finetune.yaml"
    else:
        opt.hyp = './conf/hyp.scratch.yaml'
    
    # Set DDP variables
    opt.total_batch_size = opt.batch_size
    opt.world_size = int(os.environ["WORLD_SIZE"]) if "WORLD_SIZE" in os.environ else 1
    opt.global_rank = int(os.environ["RANK"]) if "RANK" in os.environ else -1
    set_logging(opt.global_rank)

    # Resume
    if opt.resume:  # resume an interrupted run
        ckpt = (
            opt.resume if isinstance(opt.resume, str) else get_latest_run()
        )  # specified or most recent path
        log_dir = Path(ckpt).parent.parent  # runs/exp0
        assert os.path.isfile(ckpt), "ERROR: --resume checkpoint does not exist"
        # with open(log_dir / 'opt.yaml') as f:
        #     opt = argparse.Namespace(**yaml.safe_load(f, Loader=yaml.FullLoader))  # replace
        opt.cfg, opt.weights, opt.resume = "", ckpt, True
        logger.info("Resuming training from %s" % ckpt)

    else:
        # opt.hyp = opt.hyp or ('hyp.finetune.yaml' if opt.weights else 'hyp.scratch.yaml')
        opt.data, opt.cfg, opt.hyp = (
            check_file(opt.data),
            check_file(opt.cfg),
            check_file(opt.hyp),
        )  # check files
        assert len(opt.cfg) or len(
            opt.weights
        ), "either --cfg or --weights must be specified"
        opt.img_size.extend(
            [opt.img_size[-1]] * (2 - len(opt.img_size))
        )  # extend to 2 sizes (train, test)
        log_dir = increment_dir(Path(opt.logdir) / "exp", opt.name)  # runs/exp1

    device = select_device(opt.device, batch_size=opt.batch_size)

    # DDP mode
    if opt.local_rank != -1:
        assert torch.cuda.device_count() > opt.local_rank
        torch.cuda.set_device(opt.local_rank)
        device = torch.device("cuda", opt.local_rank)
        dist.init_process_group(
            backend="nccl", init_method="env://"
        )  # distributed backend
        assert (
            opt.batch_size % opt.world_size == 0
        ), "--batch-size must be multiple of CUDA device count"
        opt.batch_size = opt.total_batch_size // opt.world_size

    logger.info(opt)
    with open(opt.hyp) as f:
        # hyp = yaml.load(f, Loader=yaml.FullLoader)  # load hyps
        hyp = yaml.safe_load(f)

    # Train
    tb_writer = None
    if opt.global_rank in [-1, 0]:
        logger.info(
            'Start Tensorboard with "tensorboard --logdir %s", view at http://localhost:6006/'
            % opt.logdir
        )
        tb_writer = SummaryWriter(log_dir=log_dir)  # runs/exp0

    _, exp_name = train.train(
        hyp, 
        opt, 
        device, 
        tb_writer, 
        overwrite_epochs=overwrite_epochs, 
        freeze=freeze, 
        optimizertype=opt.optimizertype, 
        loadfrom=checkpoint_to_resume
    )
    
    _, _, val_imgfolder, val_txtfolder = folder_naming_convention(opt.datadir, opt.set_name)
    
    best_model_saved_path = "./logs/%s/weights" % exp_name  # exp_name is where the latest weights are saved in the logs directory
   
    try:
        copy2bedrock(best_model_saved_path, opt.logdir)
        print("Completed copy2bedrock function")
    except:
        print("Unable to copy artefacts from %s into Bedrock" % best_model_saved_path)
    
    precision, recall, f1 = evaluate(val_imgfolder, val_txtfolder, w_th=0.03, h_th=0.03, score_th=0.3, nms_iou=0.4)
    log_metrics(precision, recall, f1)
    return _, exp_name



def copy_weights_from_logs(expt_folder_name, new_ckpt_name):
    """
    Args:
        expt_folder_name (str): Name of logs folder where new training is saved to
        new_ckpt_name (str): Name of give to ckpt file transferred to Model folder
    """
    if new_ckpt_name.split('.')[-1]!="pt":
        new_ckpt_name = new_ckpt_name+".pt"    # append extension
    
    cmd = """cp ./logs/%s/weights/best.pt ./data/06_models/%s""" % (expt_folder_name, new_ckpt_name)
    process = subprocess.Popen(['/bin/bash', '-c', cmd])
    _, _ = process.communicate()
    print("Created ./data/06_models/{}".format(new_ckpt_name))
    




def train_standard_fine(opt, config, num_epochs_standard, num_epochs_fine, 
    train_folder, test_folder, output_name, obj_class_list, ckpt_to_load=None):
    """
    Args:
        config: Dictionary loaded from yaml file
        num_epochs_standard (int): Number of epochs for standard training
        num_epochs_fine (int): Number of epochs for finetuning
        train_folder (str): Name of trainset
        test_folder (str): Name of testset
        output_name (str): Name of output weights
        ckpt_to_load (str): Name of weights to load
    """
    if ckpt_to_load is None:
        ckpt_to_load = "yolov5%s"%config["modeltype"]
    
    update_config(config["modeltype"], train_folder, test_folder, obj_class_list)
    if opt.freeze:
        _, exp_name = pipeline_train(
            opt=opt,
            finetune=False, 
            checkpoint_to_resume=ckpt_to_load,
            overwrite_epochs=5,
            freeze=True,
        )
        copy_weights_from_logs(expt_folder_name=exp_name, new_ckpt_name=output_name)
        _, exp_name = pipeline_train(
            opt=opt,
            finetune=False, 
            checkpoint_to_resume=output_name,  # Name which checkpoint is saved (according to `new_ckpt_name` above)
            overwrite_epochs=num_epochs_standard, 
        )
    else:
        _, exp_name = pipeline_train(
            opt=opt,
            finetune=False, 
            checkpoint_to_resume=ckpt_to_load,
            overwrite_epochs=num_epochs_standard, 
        )
    copy_weights_from_logs(expt_folder_name=exp_name, new_ckpt_name=output_name)
    # _, exp_name = pipeline_train(
    #     opt=opt,
    #     finetune=True, 
    #     checkpoint_to_resume=output_name,
    #     overwrite_epochs=num_epochs_fine,
    # )
    # copy_weights_from_logs(expt_folder_name=exp_name, new_ckpt_name=output_name)

    


# def smart_train(opt):
# def main(eval_only):
    



if __name__ == "__main__":
    # smart_train(opt)
    # main(eval_only=False)
    eval_only = False
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str, default='yolov5m', help='initial weights path')
    parser.add_argument('--cfg', type=str, default='', help='model.yaml path')
    parser.add_argument('--data', type=str, default='conf/custom_5m.yaml', help='data.yaml path')
    parser.add_argument('--hyp', type=str, default='conf/hyp.scratch.yaml', help='hyperparameters path')
    parser.add_argument('--lr0', type=float, default=0.001, help='default learning rate')
    parser.add_argument('--lrf', type=float, default=0.20, help='fraction for cyclic lr')
    parser.add_argument('--logdir', type=str, default='logs/')
    parser.add_argument('--datadir', type=str, default='data/01_raw')
    parser.add_argument('--set_name', type=str, default='mergedsix')
    parser.add_argument('--best_model_saved_path', type=str, default='data/07_model_output')
    parser.add_argument('--epochs', type=int, default=30)
    parser.add_argument('--optimizertype', type=str, default="sgdn")
    parser.add_argument('--freeze', action='store_true', help='freeze all weights except final layer')
    parser.add_argument('--batch-size', type=int, default=8, help='total batch size for all GPUs')
    parser.add_argument('--img-size', nargs='+', type=int, default=[640, 640], help='[train, test] image sizes')
    parser.add_argument('--rect', action='store_true', help='rectangular training')
    parser.add_argument('--resume', nargs='?', const=True, default=False, help='resume most recent training')
    parser.add_argument('--nosave', action='store_true', help='only save final checkpoint')
    parser.add_argument('--notest', action='store_true', help='only test final epoch')
    parser.add_argument('--noautoanchor', action='store_true', help='disable autoanchor check')
    parser.add_argument('--evolve', action='store_true', help='evolve hyperparameters')
    parser.add_argument('--bucket', type=str, default='', help='gsutil bucket')
    parser.add_argument('--cache-images', action='store_true', help='cache images for faster training')
    parser.add_argument('--image-weights', action='store_true', help='use weighted image selection for training')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--multi-scale', action='store_true', help='vary img-size +/- 50%%')
    parser.add_argument('--single-cls', action='store_true', help='train as single-class dataset')
    parser.add_argument('--adam', action='store_true', help='use torch.optim.Adam() optimizer')
    parser.add_argument('--sync-bn', action='store_true', help='use SyncBatchNorm, only available in DDP mode')
    parser.add_argument('--local_rank', type=int, default=-1, help='DDP parameter, do not modify')
    parser.add_argument('--log-imgs', type=int, default=16, help='number of images for W&B logging, max 100')
    parser.add_argument('--workers', type=int, default=8, help='maximum number of dataloader workers')
    parser.add_argument('--project', default='runs/train', help='save to project/name')
    parser.add_argument('--name', default='exp', help='save to project/name')
    parser.add_argument('--exist-ok', action='store_true', help='existing project/name ok, do not increment')
    opt, _ = parser.parse_known_args()
    
    ### For use with bedrock
    params = {}
    if os.getenv("BATCH_SIZE") is not None:
        params["batch_size"] = int(os.getenv("BATCH_SIZE"))
    if os.getenv("NUM_EPOCHS") is not None:
        params["epochs"] = int(os.getenv("NUM_EPOCHS"))
    if os.getenv("LR") is not None:
        params["lr0"] = float(os.getenv("LR"))
    if os.getenv("OPTIMIZERTYPE") is not None:
        params["optimizertype"] = str(os.getenv("OPTIMIZERTYPE"))
    if os.getenv("FREEZE") is not None:
        params["freeze"] = bool(int(os.getenv("FREEZE")))    # False if '0'
    if os.getenv("SET_NAME") is not None:
        params["set_name"] = str(os.getenv("SET_NAME"))
    # if os.getenv("SET_NAME") is not None:
    #     set_name = os.getenv("SET_NAME")
    # else:
    #     set_name = "mergedsix"  #"sample5"
    
    # opt = train.get_args()
    opt = train.set_params(opt, params)   # if BATCH_SIZE or NUM_EPOCHS etc. defined in env variable, will update and overwrite opt
    
    if eval_only==False:
        if not os.path.exists("./data/06_models/%s.pt"%opt.weights):
            if opt.weights in set(["yolov5s","yolov5m","yolov5l","yolov5x"]):
                print("Downloading weights for %s" % opt.weights)
                download_weights("vama-sceneuds-images/SU21corpus/weights", opt.weights+".pt")
            else:
                print("Weights does not exist!!")
        else:
            print("Resume training with weights from ./data/06_models/%s" % opt.weights)
        
        if not os.path.exists(opt.data):
            if opt.data in set(["./conf/custom_5s.yaml","./conf/custom_5m.yaml","./conf/custom_5l.yaml","./conf/custom_5x.yaml"]):
                print("Downloading yaml config to %s" % opt.data)
                download_config("vama-sceneuds-images/SU21corpus/config", (opt.data).split("/")[-1])
            else:
                print("Yaml config file does not exist!!")
        # time.sleep(5)
        
        with open(opt.data, "r") as f:
            config = yaml.safe_load(f)

        ### Download images and labels from S3
        trainfolder = "%s_train"%opt.set_name
        testfolder = "%s_test"%opt.set_name
        
        download_data(
            [trainfolder, testfolder], 
            "vama-sceneuds-images/SU21corpus"
        )
        
        ### Perform training, followed by copy weights into Bedrock and perform evaluation
        train_standard_fine(
            opt=opt,
            config=config, 
            num_epochs_standard=opt.epochs,
            num_epochs_fine=0,
            train_folder=config["train"].split("/")[-2], 
            test_folder=config["val"].split("/")[-2],
            output_name=config["new_weights"],
            obj_class_list=config["names"]
        )
    else:
        _, _, val_imgfolder, val_txtfolder = folder_naming_convention(opt.datadir, opt.set_name)  # eg. set_name=mergedsix
        precision, recall, f1 = evaluate(val_imgfolder, val_txtfolder, w_th=0.03, h_th=0.03, score_th=0.3, nms_iou=0.4)
        log_metrics(precision, recall, f1)
    