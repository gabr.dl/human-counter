#!/bin/bash
cd ..
bandit_high=$(cat gl-sast-report.json | jq '[.vulnerabilities [] | select(.severity=="High")]' | jq '. | length')
bandit_med=$(cat gl-sast-report.json | jq '[.vulnerabilities [] | select(.severity=="Medium")]' | jq '. | length')
bandit_low=$(cat gl-sast-report.json | jq '[.vulnerabilities [] | select(.severity=="Low")]' | jq '. | length')


echo "
==============================================
          SAST Security Scan Results
=============================================="
echo "
            High Severity:   $bandit_high
            Medium Severity: $bandit_med
            Low Severity:    $bandit_low"
echo "
-----------------------------------------------
Download full report if required
Refer to Vulnerability_Url for more information
Displaying high severity issues if any...
-----------------------------------------------"

if [[ "$bandit_high" != "0" ]]; then
    jq -r '.vulnerabilities[]| select(.severity | contains("High"))| 
    {   Issue: .message,
        Severity: .severity ,
        Confidence: .confidence , 
        Filename: .location.file , 
        start_line: .location.start_line, 
        end_line: .location.end_line, 
        Vulnerability_value : .identifiers[0].value , 
        Vulnerability_Url : .identifiers[0].url
    }' gl-sast-report.json 
    echo "-----------------------------------------------"
    exit 1
fi
