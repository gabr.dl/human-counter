version = "1.0"

train {
    step training {
        image = "tensorflow/tensorflow:2.2.0-gpu"
        install = [
            "apt-get update; apt-get install ffmpeg libsm6 libxext6 -y", //opencv
            "apt-get install -y git", //to extract git hashes
            "pip3 install --upgrade pip",
            "pip3 install Cython==0.29.17 numpy==1.19.2",
            "pip3 install -r yolov5/requirements_train.txt",
        ]
        script = [{sh = ["python smart_train.py"]}]
        resources {
            cpu = "2"
            memory = "12G"
            gpu = "1"
        }
    }

    parameters {
        NUM_EPOCHS = "10"
        BATCH_SIZE = "4"
    }

    secrets = [
        "AWS_SECRET_ACCESS_KEY",
        "AWS_ACCESS_KEY_ID"
    ]
}

serve {
    image = "python:3.7"
    install = [
        "apt-get update; apt-get install ffmpeg libsm6 libxext6 -y", //opencv
        "pip3 install --upgrade pip",
        "pip install Cython==0.29.17 numpy==1.19.2",
        "pip install -r requirements.txt",
    ]
    script = [
        {sh = [
            "gunicorn --bind=:${BEDROCK_SERVER_PORT:-8080} --worker-class=gthread --workers=${WORKERS} --timeout=300 stream_app:app"
        ]}
    ]
    parameters {
        WORKERS = "1"
    }
}



