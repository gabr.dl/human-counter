"""
This script contains the main tracker function that accepts input from either video files or webcam to display the annotated video
or real-annotation of the webcam stream respectively on the terminal environment
"""
import os
import sys
import numpy as np
import argparse
import shutil
import time
from pathlib import Path

import cv2
import torch
import torch.backends.cudnn as cudnn
from PIL import Image

os.chdir('./yolov5')
from yolov5.predict import detect
from yolov5.utils.general import check_img_size, non_max_suppression, scale_coords
from yolov5.utils.torch_utils import select_device, time_synchronized
from yolov5.utils.datasets import LoadStreams, LoadImages
os.chdir('../')
from deep_sort.src.deep_sort import DeepSort
from deep_sort.utils.parser import get_config
from src.trackableobject import TrackableObject
from src.utils import bbox_rel, compute_color_for_labels, draw_boxes, linear_equation

def tracker(opt, save_img=False):
    """
    Main function
    opt - arguments

    Output:
    Video stream in terminal environment
    Annotated video output
    """
    out, source, view_img, save_txt, imgsz, weights = \
        opt.output, opt.source, opt.view_img, opt.save_txt, opt.img_size, opt.weights

    webcam = source == '0' or source.startswith(
        'rtsp') or source.startswith('http') or source.endswith('.txt')
    
    # instantiate deepsort
    cfg = get_config()
    cfg.merge_from_file(opt.config_deepsort)
    deepsort = DeepSort(cfg.DEEPSORT.REID_CKPT,
                        max_dist=cfg.DEEPSORT.MAX_DIST, min_confidence=cfg.DEEPSORT.MIN_CONFIDENCE,
                        nms_max_overlap=cfg.DEEPSORT.NMS_MAX_OVERLAP, max_iou_distance=cfg.DEEPSORT.MAX_IOU_DISTANCE,
                        max_age=cfg.DEEPSORT.MAX_AGE, n_init=cfg.DEEPSORT.N_INIT, nn_budget=cfg.DEEPSORT.NN_BUDGET,
                        use_cuda=True)

    # check devices
    device = select_device(opt.device)
    if os.path.exists(out):
        shutil.rmtree(out)  # delete output folder
    os.makedirs(out)  # make new output folder
    half = device.type != 'cpu'

    # load model
    model = torch.load(weights, map_location=device)[
        'model'].float()  # load to FP32
    model.to(device).eval()
    if half:
        model.half()  # to FP16

    # data source
    vid_path, vid_writer = None, None
    if webcam:
        view_img = True
        cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz)
    else:
        save_img = True
        dataset = LoadImages(source, img_size=imgsz)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    
    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
    # run once
    _ = model(img.half() if half else img) if device.type != 'cpu' else None

    save_path = str(Path(out))
    txt_path = str(Path(out)) + '/results.txt'

    #object counting
    trackableObjects = {}
    totalDown = 0
    totalUp = 0

    #iterate through frames in video
    for frame_idx, (path, img, im0s, vid_cap) in enumerate(dataset):
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        # Apply NMS
        pred = non_max_suppression(
            pred, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        t2 = time_synchronized()

        # line start and end
        if webcam:
            H, W = im0s[0].shape[:2]
        else:
            H, W = im0s.shape[:2]
        if not opt.line_style:
            # line_start = (30, H)
            # line_end = (W // 2, 0)
            line_start = (round(W // 2), round(H))
            line_end = (round(W // 2), round(0))
        else:
            if opt.line_style == "vertical":
                line_start = (round(W // 2), round(H))
                line_end = (round(W // 2), 0)
            elif opt.line_style == "horizontal":
                line_start = (0, round(H // 2))
                line_end = (W, round(H // 2))

        #draw line
        draw_start = line_start[0], H-line_start[1]
        draw_end = line_end[0], H-line_end[1]
        if not webcam:
            cv2.line(im0s, draw_start, draw_end, (0, 255, 255), 2)
        
        # Process detections
        for i, det in enumerate(pred):  # detections per image
            if webcam:  # batch_size >= 1
                p, s, im0 = path[i], '%g: ' % i, im0s[i].copy()
            else:
                p, s, im0 = path, '', im0s

            H, W = im0.shape[:2]

            info = [
                    ("Direction2", totalUp),
                    ("Direction1", totalDown),
                    ]
            # loop over the info tuples and draw them on our frame
            for (i, (k, v)) in enumerate(info):
                text = "{}: {}".format(k, v)
                cv2.putText(im0, text, (10, H - ((i * 20) + 20)),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

            s += '%gx%g ' % img.shape[2:]  # print string
            save_path = str(Path(out) / Path(p).name)

            if det is not None and len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(
                    img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += '%g %ss, ' % (n, names[int(c)])  # add to string

                bbox_xywh = []
                confs = []

                # Adapt detections to deep sort input format
                for *xyxy, conf, cls in det:
                    x_c, y_c, bbox_w, bbox_h = bbox_rel(*xyxy)
                    obj = [x_c, y_c, bbox_w, bbox_h]
                    bbox_xywh.append(obj)
                    confs.append([conf.item()])
                
                #convert to tensor
                xywhs = torch.Tensor(bbox_xywh)
                confss = torch.Tensor(confs)

                # Pass detections to deepsort
                outputs = deepsort.update(xywhs, confss, im0)

                # draw boxes for visualization
                if len(outputs) > 0:
                    #xyxy bounding box and numerical identities
                    bbox_xyxy = outputs[:, :4]
                    identities = outputs[:, -1]
                    # draw_boxes(im0, bbox_xyxy, identities)
                    bbox_xyxy = bbox_xyxy.tolist()
                    
                    for count, person in enumerate(bbox_xyxy):
                        #calculate centroid
                        centroidX = person[0] + ((person[2] - person[0])//2)
                        centroidY = person[3] + ((person[1] - person[3])//2)
                        centroid = [centroidX, centroidY]
                        
                        #id
                        objectID = identities[count]

                        #counter
                        to = trackableObjects.get(objectID, None)

                        direction = 0

                        #linear equation variables
                        gradient, y_incpt = linear_equation(line_start, line_end)

                        if to is None:
                            to = TrackableObject(objectID, centroid)

                        #determining directionality
                        else:
                            if gradient == "horizontal":
                                y = [c[1] for c in to.centroids]
                                direction = centroid[1] - np.mean(y)
                            elif gradient == "vertical":
                                x = [c[0] for c in to.centroids]
                                direction = centroid[0] - np.mean(x)
                            elif abs(gradient) > 1:
                                x = [c[0] for c in to.centroids]
                                direction = centroid[0] - np.mean(x)
                            elif abs(gradient) <= 1:
                                y = [c[1] for c in to.centroids]
                                direction = centroid[1] - np.mean(y)
                            to.centroids.append(centroid)

                        #counting criteria
                        #for horizontal line
                        if gradient == "horizontal":
                            y_incpt = round(y_incpt)
                            if not to.counted and centroid[1] in range(y_incpt-5, y_incpt+6):
                                if direction < 0:
                                    totalUp += 1
                                    to.counted = True
                                elif direction > 0:
                                    totalDown += 1
                                    to.counted = True
                        #for vertical line
                        elif gradient == "vertical":
                            line_starter = round(line_start[0])
                            if not to.counted and centroid[0] in range(line_starter-5, line_starter+6):
                                if direction < 0:
                                    totalUp += 1
                                    to.counted = True
                                elif direction > 0:
                                    totalDown += 1
                                    to.counted = True
                        #for diagonals
                        elif type(gradient) == float:
                            #closer to verticality
                            if abs(gradient) > 1:
                                equation_x = round((H-centroid[1]-y_incpt)/gradient)
                                if not to.counted and (centroid[0] in range(equation_x-5, equation_x+6) 
                                and centroid[1] in range(min(line_start[1], line_end[1]), max(line_start[1], line_end[1])+1)):
                                    if direction < 0:
                                        totalUp += 1
                                        to.counted = True
                                    elif direction > 0:
                                        totalDown += 1
                                        to.counted = True
                            #closer to horizontal
                            if abs(gradient) <= 1:
                                equation_y = H-round((centroid[0]*gradient) + y_incpt)
                                # equation_x = round((centroid[1]-y_incpt)/gradient)
                                if not to.counted and (centroid[1] in range(equation_y-5, equation_y+6) 
                                and centroid[0] in range(min(line_start[0], line_end[0]), max(line_start[0], line_end[0])+1)):
                                    if direction < 0:
                                        totalUp += 1
                                        to.counted = True
                                    elif direction > 0:
                                        totalDown += 1
                                        to.counted = True
                                        
                        # store the trackable object in our dictionary
                        trackableObjects[objectID] = to

                        #draw frame
                        text = "ID {}".format(objectID)
                        cv2.putText(im0, text, (centroid[0] - 10, centroid[1] - 10),
                            cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 255, 0), 1)
                        cv2.circle(im0, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
                        if webcam:
                            cv2.line(im0, draw_start, draw_end, (0, 255, 255), 2)

                # Write MOT compliant results to file
                if save_txt and len(outputs) != 0:
                    for j, output in enumerate(outputs):
                        bbox_left = output[0]
                        bbox_top = output[1]
                        bbox_w = output[2]
                        bbox_h = output[3]
                        identity = output[-1]
                        with open(txt_path, 'a') as f:
                            f.write(('%g ' * 10 + '\n') % (frame_idx, identity, bbox_left,
                                                           bbox_top, bbox_w, bbox_h, -1, -1, -1, -1))  # label format

            else:
                deepsort.increment_ages()

            # Print time (inference + NMS)
            print('%sDone. (%.3fs)' % (s, t2 - t1))

            # Stream results
            if view_img:
                cv2.imshow(p, im0)
                if cv2.waitKey(1) == ord('q'):  # q to quit
                    raise StopIteration

            # Save results (image with detections)
            if save_img:
                print('saving img!')
                if dataset.mode == 'images':
                    cv2.imwrite(save_path, im0)
                else:
                    print('saving video!')
                    if vid_path != save_path:  # new video
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            vid_writer.release()  # release previous video writer

                        fps = vid_cap.get(cv2.CAP_PROP_FPS)
                        w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                        h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        vid_writer = cv2.VideoWriter(
                            save_path, cv2.VideoWriter_fourcc(*opt.fourcc), fps, (w, h))
                    vid_writer.write(im0)
                    
    if save_txt or save_img:
        print('Results saved to %s' % os.getcwd() + os.sep + out)
        # if platform == 'darwin':  # MacOS
            # os.system('open ' + save_path)

    print('Done. (%.3fs)' % (time.time() - t0))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str,
                        default='yolov5/data/06_models/yolov5m.pt', help='model.pt path')
    # file/folder, 0 for webcam
    parser.add_argument('--source', type=str,
                        default='inference/images', help='source')
    parser.add_argument('--output', type=str, default='inference/output',
                        help='output folder')  # output folder
    parser.add_argument('--img-size', type=int, default=640,
                        help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float,
                        default=0.3, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float,
                        default=0.5, help='IOU threshold for NMS')
    parser.add_argument('--fourcc', type=str, default='mp4v',
                        help='output video codec (verify ffmpeg support)')
    parser.add_argument('--device', default='',
                        help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', type=bool,
                        default=False, help='display results')
    parser.add_argument('--save-txt', action='store_true',
                        help='save results to *.txt')
    parser.add_argument('--classes', nargs='+', type=int,
                        default=[0], help='filter by class')
    parser.add_argument('--save-video', action='store_true',
                        help='save output video')
    parser.add_argument('--agnostic-nms', action='store_true',
                        help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true',
                        help='augmented inference')
    parser.add_argument("--config_deepsort", type=str,
                        default="deep_sort/configs/deep_sort.yaml")
    parser.add_argument("--line_style", type=str,
                        default=())
    args = parser.parse_args()
    args.img_size = check_img_size(args.img_size)
    # print(args)

    with torch.no_grad():
        tracker(args)