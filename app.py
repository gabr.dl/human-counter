"""
This script contains the main streamlit app that allows users to upload videos for object counting tasks.
Outputs include a downloadable annotated video file and the annotated video file that is displayed real-time on the streamlit app.
"""
import os
import time
from pathlib import Path
import tempfile
import numpy as np
import pickle
import glob
import base64

from PIL import Image
import streamlit as st
from streamlit_drawable_canvas import st_canvas
import cv2
import torch
import torch.backends.cudnn as cudnn
import imageio

os.chdir('./yolov5')
from predict import detect
from utils.general import check_img_size, non_max_suppression, scale_coords
from utils.torch_utils import select_device, time_synchronized
from utils.datasets import LoadStreams, LoadImages, letterbox
os.chdir('../')
from deep_sort.src.deep_sort import DeepSort
from deep_sort.utils.parser import get_config
from src.trackableobject import TrackableObject
from src.utils import bbox_rel, compute_color_for_labels, draw_boxes, linear_equation

def hide_navbar():
    """hide navbar so its not apparent this is from streamlit"""
    hide_streamlit_style = """
    <style>
    #MainMenu {visibility: hidden;}
    footer {visibility: hidden;}
    </style>
    """
    st.markdown(hide_streamlit_style, unsafe_allow_html=True) 

def main():
    """Main tracker function"""
    st.title("Human Counter")

    st.sidebar.title("Options")
    conf_thres = st.sidebar.slider("Confidence Threshold", min_value=0.1, max_value=1.0, value=0.3, step=0.1)
    iou_thres = st.sidebar.slider("IoU Threshold", min_value=0.1, max_value=1.0, value=0.5, step=0.1)
    display_video = st.sidebar.checkbox("Display Video")
    download_video = st.sidebar.checkbox("Download Video")
    # line_orientation = st.selectbox("Boundary Line Orientation", ("Vertical", "Horizontal", "Down Diagonal", "Up Diagonal"))

    uploaded_file = st.file_uploader("Upload a video.", type=['mp4'])
    if uploaded_file is not None:

        #get first image of video
        video_name = uploaded_file.name.replace(".mp4", "")
        tfile = tempfile.NamedTemporaryFile(delete=False)
        tfile.write(uploaded_file.read())
        cap = cv2.VideoCapture(tfile.name)
        ret, frame = cap.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        reduction_factor = frame.shape[1]/800

        #annotate on image
        canvas_result = st_canvas(
            fill_color="rgba(255, 165, 0, 0.3)",  # Fixed fill color with some opacity
            stroke_width=2,
            stroke_color="#0bfc03",
            background_image=Image.fromarray(frame.astype('uint8'), 'RGB'),
            height=frame.shape[0]//reduction_factor,
            width=frame.shape[1]//reduction_factor,
            drawing_mode="line",
            key="canvas"
        )

        line_start = [0,0]
        line_end = [0,0]

        if np.any(canvas_result.image_data != [0,0,0,0]):
            #calculate start and end points of line drawn
            non_black_pixels = np.where(canvas_result.image_data != [0,0,0,0])
            if non_black_pixels[0].any():
                line_start = [int(round(non_black_pixels[1][0]*reduction_factor)), int(round(non_black_pixels[0][0]*reduction_factor))]
                line_end = [int(round(non_black_pixels[1][-1]*reduction_factor)), int(round(non_black_pixels[0][-1]*reduction_factor))]
        
            # video = cv2.VideoWriter("./video.avi", 0, 1, (frame.shape[1],frame.shape[0]))
            video_slot = st.empty()
            
            #pass line end and start into main function
            weights = "yolov5/data/06_models/yolov5m.pt"
            agnostic_nms = True
            augment = True
            config_deepsort = "deep_sort/configs/deep_sort.yaml"
            classes = [0]

            #instantiate deepsort
            cfg = get_config()
            cfg.merge_from_file(config_deepsort)
            deepsort = DeepSort(os.path.abspath(cfg.DEEPSORT.REID_CKPT),
                                max_dist=cfg.DEEPSORT.MAX_DIST, min_confidence=cfg.DEEPSORT.MIN_CONFIDENCE,
                                nms_max_overlap=cfg.DEEPSORT.NMS_MAX_OVERLAP, max_iou_distance=cfg.DEEPSORT.MAX_IOU_DISTANCE,
                                max_age=cfg.DEEPSORT.MAX_AGE, n_init=cfg.DEEPSORT.N_INIT, nn_budget=cfg.DEEPSORT.NN_BUDGET,
                                use_cuda=True)

            #check devices
            device = select_device("")
            half = device.type != 'cpu'

            #load model
            model = torch.load(weights, map_location=device)[
                'model'].float()  # load to FP32
            model.to(device).eval()
            if half:
                model.half()  # to FP16

            # Get names and colors
            names = model.module.names if hasattr(model, 'module') else model.names

            # Run inference
            t0 = time.time()
            img = torch.zeros((1, 3, 640, 640), device=device)  # init img
            # run once
            _ = model(img.half() if half else img) if device.type != 'cpu' else None

            ret = True

            with open("./image_index.txt", "r") as f:
                image_dex = int(f.read())
            
            if line_start != [0,0]:
                while ret:
                    ret, im0 = cap.read()
                    image_dex += 1
                    if not ret:
                        break
                    # Padded resize
                    img = letterbox(im0, new_shape=640)[0]

                    # Convert
                    img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
                    img = np.ascontiguousarray(img)

                    img = torch.from_numpy(img).to(device)
                    img = img.half() if half else img.float()  # uint8 to fp16/32
                    img /= 255.0  # 0 - 255 to 0.0 - 1.0
                    if img.ndimension() == 3:
                        img = img.unsqueeze(0)

                    # Inference
                    t1 = time_synchronized()
                    pred = model(img, augment=True)[0]

                    # Apply NMS
                    pred = non_max_suppression(
                        pred, conf_thres, iou_thres, classes=[], agnostic=True)
                    t2 = time_synchronized()

                    #line start and end
                    H, W = im0.shape[:2]

                    #draw line
                    draw_start = line_start[0], line_start[1]
                    draw_end = line_end[0], line_end[1]
                    cv2.line(im0, draw_start, draw_end, (0, 255, 255), 2)

                    # Process detections
                    for i, det in enumerate(pred):  # detections per image
                        s, im0 = '', im0

                        H, W = im0.shape[:2]

                        with open("./count.txt", "r") as f:
                            count_file = f.readlines()[0]
                            totalDown = int(count_file[0])
                            totalUp = int(count_file[1])

                        #object counting
                        trackableObjects = {}
                        if Path("./counter_dict.pkl").exists():
                            with open("./counter_dict.pkl", "rb") as f:
                                trackableObjects = pickle.load(f)
                        else:
                            with open("./counter_dict.pkl", "wb") as f:
                                pickle.dump(trackableObjects, f, pickle.HIGHEST_PROTOCOL)
                        
                        info = [
                                ("Direction2", totalUp),
                                ("Direction1", totalDown),
                                ]
                        # loop over the info tuples and draw them on our frame
                        for (i, (k, v)) in enumerate(info):
                            text = "{}: {}".format(k, v)
                            cv2.putText(im0, text, (10, H - ((i * 20) + 20)),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 2)

                        s += '%gx%g ' % img.shape[2:]  # print string

                        if det is not None and len(det):
                            # Rescale boxes from img_size to im0 size
                            det[:, :4] = scale_coords(
                                img.shape[2:], det[:, :4], im0.shape).round()

                            bbox_xywh = []
                            confs = []

                            # Adapt detections to deep sort input format
                            for *xyxy, conf, cls in det:
                                x_c, y_c, bbox_w, bbox_h = bbox_rel(*xyxy)
                                obj = [x_c, y_c, bbox_w, bbox_h]
                                bbox_xywh.append(obj)
                                confs.append([conf.item()])

                            #convert to tensor
                            xywhs = torch.Tensor(bbox_xywh)
                            confss = torch.Tensor(confs)

                            # Pass detections to deepsort
                            outputs = deepsort.update(xywhs, confss, im0)

                            # draw boxes for visualization
                            if len(outputs) > 0:
                                #xyxy bounding box and numerical identities
                                bbox_xyxy = outputs[:, :4]
                                identities = outputs[:, -1]
                                # draw_boxes(im0, bbox_xyxy, identities)
                                bbox_xyxy = bbox_xyxy.tolist()
                                
                                for count, person in enumerate(bbox_xyxy):
                                    #calculate centroid
                                    centroidX = person[0] + ((person[2] - person[0])//2)
                                    centroidY = person[3] + ((person[1] - person[3])//2)
                                    centroid = [centroidX, centroidY]
                                    
                                    #id
                                    objectID = identities[count]

                                    #counter
                                    to = trackableObjects.get(objectID, None)

                                    direction = 0

                                    #linear equation variables
                                    gradient, y_incpt = linear_equation(line_start, line_end)
                                    if to is None:
                                        to = TrackableObject(objectID, centroid)

                                    #determining directionality
                                    if gradient == "horizontal":
                                        y = [c[1] for c in to.centroids]
                                        direction = centroid[1] - np.mean(y)
                                    elif gradient == "vertical":
                                        x = [c[0] for c in to.centroids]
                                        direction = centroid[0] - np.mean(x)
                                    elif abs(gradient) > 1:
                                        x = [c[0] for c in to.centroids]
                                        direction = centroid[0] - np.mean(x)
                                    elif abs(gradient) <= 1:
                                        y = [c[1] for c in to.centroids]
                                        direction = centroid[1] - np.mean(y)

                                    to.centroids.append(centroid)

                                    #counting criteria
                                    #for horizontal line
                                    if gradient == "horizontal":
                                        y_incpt = round(y_incpt)
                                        if not to.counted and centroid[1] in range(y_incpt-5, y_incpt+6):
                                            if direction < 0:
                                                totalUp += 1
                                                to.counted = True
                                            elif direction > 0:
                                                totalDown += 1
                                                to.counted = True
                                    #for vertical line
                                    elif gradient == "vertical":
                                        line_start[0] = round(line_start[0])
                                        if not to.counted and centroid[0] in range(line_start[0]-5, line_start[0]+6):
                                            if direction < 0:
                                                totalUp += 1
                                                to.counted = True
                                            elif direction > 0:
                                                totalDown += 1
                                                to.counted = True
                                    #for diagonals
                                    else:
                                        #closer to verticality
                                        gradient = float(gradient)
                                        if abs(gradient) > 1:
                                            equation_x = int(round((centroid[1]-y_incpt)/gradient))
                                            if not to.counted and (centroid[0] in range(equation_x-5, equation_x+6) 
                                            and centroid[1] in range(min(line_start[1], line_end[1]), max(line_start[1], line_end[1])+1)):
                                                if direction < 0:
                                                    totalUp += 1
                                                    to.counted = True
                                                elif direction > 0:
                                                    totalDown += 1
                                                    to.counted = True
                                        #closer to horizontal
                                        if abs(gradient) <= 1:
                                            equation_y = int(round((centroid[0]*gradient) + y_incpt))
                                            # equation_x = round((centroid[1]-y_incpt)/gradient)
                                            if not to.counted and (centroid[1] in range(equation_y-5, equation_y+6)
                                            and centroid[0] in range(min(line_start[0], line_end[0]), max(line_start[0], line_end[0])+1)):
                                                if direction < 0:
                                                    totalUp += 1
                                                    to.counted = True
                                                elif direction > 0:
                                                    totalDown += 1
                                                    to.counted = True
                                                    
                                    # store the trackable object in our dictionary
                                    trackableObjects[objectID] = to

                                    with open("./counter_dict.pkl", "wb") as f:
                                        pickle.dump(trackableObjects, f, pickle.HIGHEST_PROTOCOL)

                                    #write up and down in text file
                                    with open("./count.txt", "w") as f:
                                        f.write(f"{totalDown}{totalUp}")

                                    #draw frame
                                    text = "ID {}".format(objectID)
                                    cv2.putText(im0, text, (centroid[0] - 10, centroid[1] - 10),
                                        cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 255, 0), 1)
                                    cv2.circle(im0, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
                        
                        else:
                            deepsort.increment_ages()
                        
                        if display_video:
                            im0 = cv2.cvtColor(im0, cv2.COLOR_RGB2BGR)
                            video_slot.image(im0)
                    
                    if download_video:
                        #save image into cache folder
                        im0 = cv2.cvtColor(im0, cv2.COLOR_RGB2BGR)
                        im0 = Image.fromarray(im0)
                        if not os.path.exists("image_cache"):
                            os.makedirs("image_cache")
                        im0.save(f"./image_cache/image_{image_dex}.jpg")

                        #update image index
                        with open("./image_index.txt", "w") as f:
                            f.write(f"{image_dex}")

        if download_video:
            #write video file
            with imageio.get_writer("./movie.mp4", mode="I") as writer:
                with open("./image_index.txt", "r") as f:
                    total_frames = int(f.read())
                for dex in range(1, total_frames+1):
                    image_read = imageio.imread(os.path.join("image_cache", f"image_{dex}.jpg"))
                    writer.append_data(image_read)        
            try:
                video_file = open("./movie.mp4", "rb")
                st.markdown('<hr>', unsafe_allow_html=True)
                st.subheader("Download video")
                video_bytes = video_file.read()
                b64_encoded = base64.b64encode(video_bytes).decode()
                st.markdown(f'<a href="data:application/octet-stream;base64,{b64_encoded}" download="./movie.mp4">Click Here</a>', unsafe_allow_html=True)
            except Exception as e:
                print(e)
                       

if __name__ == "__main__":
    
    #Page title
    st.set_page_config(page_title='Human Counter')

    #hide navbar
    hide_navbar()

    #main program
    with torch.no_grad():
        main()
    
    #reset caches
    with open("./counter_dict.pkl", "wb") as f:
        pickle.dump({}, f, pickle.HIGHEST_PROTOCOL)
    
    with open("./count.txt", "w") as f:
        f.write("00")
    
    with open("./image_index.txt", "w") as f:
        f.write("0")
    
    files = glob.glob("./image_cache/*")
    for f in files:
        os.remove(f)